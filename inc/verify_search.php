<?php

add_action( 'admin_post_nopriv_form_search', 'do_search_verify' );
add_action( 'admin_post_form_search', 'do_search_verify' );


function do_search_verify() {
	$search = $_GET['s'];
	if(strlen($search) < 4) {
			echo "<script type='text/JavaScript'>alert('A pesquisa deve conter no mínimo 4 caracteres.'); window.history.back();</script>";
	}else {
		$search =  str_replace(' ', '%20', $search);
		wp_redirect('/?s='.$search);
	}
}