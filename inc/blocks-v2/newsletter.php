<?php

function newsletterBlock($cssClass = '', $titleTag = 'h1') {
    ?>
    <section class="nv-newsletter<?php if($cssClass) echo ' ' . $cssClass; ?>">
        <div class="nv-newsletter-content">
            <<?php echo $titleTag; ?> class="nv-newsletter-title">
                <span>Sua dose diária de notícias</span>
            </<?php echo $titleTag; ?>>
            <p class="nv-newsletter-intro">Se atualize sobre o mercado financeiro em 5 minutos!</p>
            <form class="nv-form nv-newsletter-form" id="navve-newsletter-form" method="post">
                <input type="email" name="email" class="nv-input nv-input-email" placeholder="Ex.: usuario@provedor.com.br"/>
                <input type="hidden" name="action" value="navve_newsletter_form"/>
                <span class="nv-input-error"></span>
                <button class="nv-bt nv-bt-beta" id="nv-bt-submit" type="submit"><span>Inscrever-se</span></button>
            </form>
        </div>
    </section>
    <?php
}
