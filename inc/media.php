<?php

namespace Torcedor\Media;

function get_image_types()
{
    return array(
        'image/jpg',
        'image/jpeg',
        'image/gif',
        'image/png',
    );
}

function is_image($file_handler)
{
    $file_info = @getimagesize($_FILES[ $file_handler ][ 'tmp_name' ]);
    if (empty($file_info)) {
        return false;
    }

    return in_array($file_info[ 'mime' ], get_image_types());
}

function get_vimeo_data($url)
{
    $parts = parse_url($url);

    if (!$parts[ 'host' ]) {
        return false;
    }

    if (strpos(strtolower($parts[ 'host' ]), 'vimeo.com') === false) {
        return false;
    }

    $video_id = substr($parts[ 'path' ], 1);

    $data = array();
    $data[ 'url' ] = 'http://www.vimeo.com/'.$video_id;

    $contents = file_get_contents('http://vimeo.com/api/v2/video/'.$video_id.'.json');
    $video_data = json_decode($contents, true);

    if (is_array($video_data) && isset($video_data[ 0 ][ 'thumbnail_large' ])) {
        $data[ 'thumbnail' ] = $video_data[ 0 ][ 'thumbnail_large' ];
    }

    return $data;
}

function get_youtube_data($url)
{
    $parts = parse_url($url);

    if (!$parts[ 'host' ]) {
        return false;
    }

    $host = strtolower($parts[ 'host' ]);

    if (strpos($host, 'youtube.com') !== false) {
        $path = substr($parts[ 'path' ], 1);
        $q = wp_parse_args($parts[ 'query' ]);

        if (strpos($path, 'watch') === 0) {
            $video_id = $q[ 'v' ];
        } elseif (strpos($path, 'v/') === 0 || strpos($path, 'embed/') === 0) {
            $rpos = strrpos($path, '/');
            if ($rpos !== false) {
                $video_id = substr($path, $rpos + 1);
            }
        } elseif (isset($parts[ 'fragment' ])) {
            $rpos = strrpos($parts[ 'fragment' ], '/');
            if ($rpos !== false) {
                $video_id = substr($parts[ 'fragment' ], $rpos + 1);
            }
        }
    } elseif (strpos($host, 'youtu.be') !== false) {
        $video_id = substr($parts[ 'path' ], 1);
    }

    if (!$video_id) {
        return false;
    }

    $data = array();
    $data[ 'url' ] = 'http://www.youtube.com/watch?v='.$video_id;
    $data[ 'thumbnail' ] = 'http://img.youtube.com/vi/'.$video_id.'/0.jpg';

    return $data;
}

class Manager
{
    public function __construct()
    {
        require_once ABSPATH.'wp-admin/includes/file.php';
        require_once ABSPATH.'wp-admin/includes/image.php';
        require_once ABSPATH.'wp-admin/includes/media.php';
    }

    public function attach($file_handler, $post_id = 0)
    {
        if ($_FILES[ $file_handler ][ 'error' ] !== UPLOAD_ERR_OK) {
            return false;
        }

        $id = \media_handle_upload($file_handler, $post_id);

        if (\is_wp_error($id)) {
            return false;
        }

        return $id;
    }

    public function download_and_attach($url, $post_id = 0, $title = '', $file = array())
    {
        if (count($file) == 0) {
            $file[ 'tmp_name' ] = \download_url($url, 10);
            if (is_wp_error($file[ 'tmp_name' ])) {
                return false;
            }

            $file[ 'name' ] = basename($url);
        }

        if ($title == '') {
            $desc = $this->get_ext($url);
        } else {
            $desc = $title;
        }

        /*$pathparts = pathinfo( $file[ 'tmp_name' ] );
        if ( $pathparts[ 'extension' ] == '' ) {
            $ext = $this->get_ext( $file[ 'tmp_name' ] );
            rename( $file[ 'tmp_name' ], $file[ 'tmp_name' ] . $ext );
            $file[ 'name' ] = basename( $file[ 'tmp_name' ] ) . $ext;
            $file[ 'tmp_name' ] .= $ext;
        }*/

        $id = \media_handle_sideload($file, $post_id, $desc);

        if (\is_wp_error($id)) {
            @unlink($file[ 'tmp_name' ]);

            return false;
        }

        return $id;
    }

    public function edit_attachment($attachment_id, $tw = 144, $th = 144)
    {
        $attachment = get_post($attachment_id);
        $meta = \wp_get_attachment_metadata($attachment_id);
        $file = \get_attached_file($attachment_id);

        $imgUrl = get_home_path().$_POST['imgUrl'];
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];
        // offsets
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        // crop box
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];
        // rotation angle
        $angle = $_POST['rotation'];


        $what = getimagesize($file);
        $mime = $what['mime'];
        $simg = $this->create_image($file, $mime);
        if ($simg == null) {
            return false;
        }

        // resize the original image to size of editor
        $resizedImage = imagecreatetruecolor($imgW, $imgH);
        imagecopyresampled($resizedImage, $simg, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
        // rotate the rezized image
        $rotated_image = imagerotate($resizedImage, -$angle, 0);
        // find new width & height of rotated image
        $rotated_width = imagesx($rotated_image);
        $rotated_height = imagesy($rotated_image);
        // diff between rotated & original sizes
        $dx = $rotated_width - $imgW;
        $dy = $rotated_height - $imgH;
        // crop rotated image to fit into original rezized rectangle
        $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
        imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
        imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
        // crop image into selected area
        $final_image = imagecreatetruecolor($cropW, $cropH);
        imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
        imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);

        $dimg = $final_image;

        $this->output_image($dimg, $file, $mime);

        $uploadpath = \wp_upload_dir();

        // remove intermediate and backup images if there are any
        foreach (get_intermediate_image_sizes() as $size) {
            if ($intermediate = image_get_intermediate_size($attachment_id, $size)) {
                $intermediate_file = apply_filters('wp_delete_file', $intermediate[ 'path' ]);
                @unlink(path_join($uploadpath[ 'basedir' ], $intermediate_file));
            }
        }

        $meta = \wp_generate_attachment_metadata($attachment_id, $file);
        \wp_update_attachment_metadata($attachment_id, $meta);

        return true;
    }

    private function correct_coords($meta, $data)
    {
        $w = $meta[ 'width' ];
        $mw = $data['mw'];
        if ($w <= $mw) {
            return $data;
        }
        $r = $w / $mw;

        $data[ 'x' ] = $data[ 'x' ] * $r;
        $data[ 'y' ] = $data[ 'y' ] * $r;
        $data[ 'w' ] = $data[ 'w' ] * $r;
        $data[ 'h' ] = $data[ 'h' ] * $r;

        return $data;
    }

    private function create_image($file, $mime)
    {
        $src = null;
        switch ($mime) {
            case 'image/jpeg':
                $src = imagecreatefromjpeg($file);
                break;
            case 'image/png';
                $src = imagecreatefrompng($file);
                break;
            case 'image/bmp';
                $src = imagecreatefromwbmp($file);
                break;
            case 'image/gif';
                $src = imagecreatefromgif($file);
                break;
        }

        return $src;
    }

    private function get_ext($file)
    {
        $base = basename($file);
        $ext = substr($base, strrpos($base, '.'));

        return $ext;
    }

    private function output_image($dimg, $file, $mime)
    {
        switch ($mime) {
            case 'image/jpeg':
                imagejpeg($dimg, $file);
                break;
            case 'image/png';
                imagealphablending($dimg, false);
                imagesavealpha($dimg, true);
                imagepng($dimg, $file);
                break;
            case 'image/bmp';
                imagewbmp($dimg, $file);
                break;
            case 'image/gif';
                imagegif($dimg, $file);
                break;
        }
    }
}
