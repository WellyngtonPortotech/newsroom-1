<?php
require_once get_template_directory() . '/inc/backend/content-proxy.php';
require_once get_template_directory() . '/inc/blocks/article-teaser.php';

/**
 * Prints a category block with links to posts.
 * @author Lanza
 * @param $postsParams array Parameters to get category posts.
 * @param $showCategory bool Whether to show the category label.
 * @param $thumbSize string Name of thumbnail image's size (one of the list defined in functions.php).
 * @param $titleTag string Tag to use in section title.
 */
function categoryList($postsParams = array(), $showCategory = false, $thumbSize = 'nv-list', $titleTag = 'h1') {
    $posts = call_user_func_array(array('ContentProxy', 'getPosts'), $postsParams);
    if(count($posts['posts'])){       
        ?>
        <section class="category-list">
            <header class="category-list-header">
                <<?php echo $titleTag; ?>><?php echo esc_html($posts['title']); ?></<?php echo $titleTag; ?>>
            </header>
            <div class="category-list-body">
                <?php
                $wpQuery = $posts['wpQuery'];
                while($wpQuery->have_posts()) {
                    $wpQuery->the_post();
                    articleTeaser(true, $showCategory, true, '', $thumbSize);
                }
                wp_reset_postdata();
                ?>
            </div>
            <?php
                if($posts['pages'] > $posts['page']) {
                $extra = isset($postsParams[4]) ? json_encode($postsParams[4]) : '';
                ?>
                <footer class="category-list-footer">
                    <p class="list-load-trigger" data-url="<?php echo htmlspecialchars(admin_url('admin-ajax.php'), ENT_QUOTES, 'UTF-8'); ?>"
                       data-id-page="<?php echo esc_attr($postsParams[0]); ?>"
                       data-id-section="<?php echo esc_attr($postsParams[1]); ?>"
                       data-posts-per-page="<?php echo esc_attr($postsParams[2]); ?>"
                       data-offset="<?php echo esc_attr($postsParams[3] + $postsParams[2]); ?>"
                       data-max-pages="<?php echo esc_attr($posts['pages']); ?>"
                       data-extra="<?php echo esc_attr($extra); ?>">
                        <i class="ic-plus"></i>
                        <i class="ic-spinner"></i>
                        <span class="list-load-text">Mais notícias</span>
                    </p>
                </footer>
                <?php
            }
            ?>
        </section>
        <?php
    }
}
