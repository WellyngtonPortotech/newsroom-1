<?php

function categoriesList() {
    $categories = get_categories(array(
        'orderby' => 'date',
        'order' => 'DESC',
        'exclude' => array('156254', '156255', '156256', '156257')
    ));
    $nCategories = count($categories);
    if($nCategories) {
        ?>
        <div class="nv-categories nv-scroller">
            <div class="nv-categories-content nv-scroller-area">
                <ul class="nv-categories-list nv-scroller-track">
                    <?php
                    foreach($categories as $cat) {
                        ?>
                        <li class="nv-categories-list-item">
                            <a href="<?php echo esc_attr(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <?php
    }
    ?>
    <?php
}

