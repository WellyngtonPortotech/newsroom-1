<?php

require_once get_template_directory() . '/inc/blocks/category-list.php';
get_header();

global $current_user;
get_currentuserinfo();

$author = get_user_by('slug', get_query_var('author_name'));
$author_id = $author->ID;
$facebook = get_user_meta($author->ID, 'facebook');
$facebook = $facebook[0];
if(empty($facebook)){
    $facebook = "";
}else{
    if(strpos($facebook, 'http') !== false && strpos($facebook, 'https') === false){
        $facebook = str_replace('http', 'https', $facebook);
    }else if(strpos($facebook, 'http') === false && strpos($facebook, 'https') === false){
        $facebook = 'https://' . $facebook;
    }
}
$twitter = get_user_meta($author->ID, 'twitter');
$twitter = $twitter[0];
if(empty($twitter)){
    $twitter = "";
}else{
    $twitter = "https://twitter.com/" . $twitter;
}
$instagram = get_user_meta($author->ID, 'instagram');
$instagram = $instagram[0];
if(empty($instagram)){
    $instagram = "";
}else{
    if(strpos($instagram, 'http') !== false && strpos($instagram, 'https') === false){
        $instagram = str_replace('http', 'https', $instagram);
    }else if(strpos($instagram, 'http') === false && strpos($instagram, 'https') === false){
        $instagram = 'https://' . $instagram;
    }
}
$linkedin = get_user_meta($author->ID, 'linkedin');
$linkedin = $linkedin[0];
if(empty($linkedin)){
    $linkedin = "";
}else{
    if(strpos($linkedin, 'http') !== false && strpos($linkedin, 'https') === false){
        $linkedin = str_replace('http', 'https', $linkedin);
    }else if(strpos($linkedin, 'http') === false && strpos($linkedin, 'https') === false){
        $linkedin = 'https://' . $linkedin;
    }
}
?>
<div class="site-body site-author">
    <div id="main-profile-author">
        <div class="archive author-page post-type-author authorid-<?php echo $author_id; ?>">
            <div id="inside-author-info">
                <div class="inside-author-image">
                    <?php echo wp_kses_post(get_avatar($author->ID, '150')); ?>
                </div>
                <div id="inside-author-desc">
                    <h1>
                        <?php echo esc_html($author->display_name); ?>
                    </h1>
                <ul class="inside-author-social-nets">
                    <?php 
                        if($instagram){
                            echo "<li><a href='$instagram'><span>Instagram</span></a></li>";
                        }
                        if($twitter){
                            echo "<li><a href='$twitter'><span>Twitter</span></a></li>";
                        }
                        if($facebook){
                            echo "<li><a href='$facebook'><span>Facebook</span></a></li>";
                        }
                        if($linkedin){
                            echo "<li><a href='$linkedin'><span>LinkedIn</span></a></li>";
                        }
                    ?>
                </ul>
                    <div class="description-author">
                        <?php echo esc_html($author->description); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (!have_posts()) {
            ?>
            <div class="widget-title">
                 Você ainda não escreveu notícias.
            </div>
            <?php
        }
        ?>
    </div>
    <div class="category-sections">
        <div class="category-more">
            <?php
                categoryList(array('author', 'author-list-01', 6, 0, array('author' => $author->ID)), false, 'nv-list', 'h2');
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>