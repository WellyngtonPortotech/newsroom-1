<?php
get_header();
?>
<div class="site-body site-404">
    <div class="content-block">
        <h1><span>Erro 404</span></h1>
        <p>Página não encontrada.</p>
        <p>Vá para a página inicial e veja todo o conteúdo do <?php bloginfo('name'); ?>.</p>
        <p><a class="nv-bt nv-bt-alpha bt-goto-home" href="<?php echo esc_url(home_url()); ?>">Ir para página inicial</a></p>
    </div>
</div>
<?php get_footer(); ?>

