<?php

require_once(get_template_directory() . '/lib/server/ThemeFunctions.php');
require_once(get_template_directory() . '/inc/blocks/article-teaser.php');
require_once(get_template_directory() . '/inc/category-colorization/category-colorization.php');
require_once(get_template_directory() . '/inc/meta-boxes/meta-boxes.php');
require_once(get_template_directory() . '/shortcode.php');
require_once(get_template_directory() . '/inc/api/rdstation-integration.php');
require_once(get_template_directory() . '/inc/verify_search.php');
require_once(get_template_directory() . '/inc/backend/content-proxy.php');
require_once(get_template_directory() . '/inc/index.php');                          // PHP library file
require_once(get_template_directory() . '/inc/media.php');                          // Add feature that change and crop avatar image collaborator

function setupConstants() {
    define("CUSTOM_TAXONOMIES", serialize(array("brand","subject")));
}

function setupFeatures() {
    add_theme_support('title-tag');
}

/* Add image sizes that user can choose */
function imageSizesToChoose($sizes) {
    return array_merge($sizes, array(
        'nv-article' => 'Foto na matéria'
    ));
}

function buildUrlPath($data){
        $base = dirname($data["path"]);
        return $data["scheme"].'://'. $data["host"] .$base;
}

/* Menus */
function setupMenu() {
    register_nav_menu('main-menu', 'Menu principal');
    register_nav_menu('header-ctas', 'CTAs de cabeçalho');
    register_nav_menu('footer-menu', 'Menu de rodapé');
    register_nav_menu('social-menu', 'Links para redes sociais');
    register_nav_menu('group-sites-menu', 'Links para outros sites');
}

function getLoginPagesSlugs() {
    return array('login', 'logout', 'register', 'lostpassword', 'resetpass');
}

/* Css files */
function insertStylesheets() {
    $tplDirUri = get_template_directory_uri();
    wp_enqueue_style('webfont', 'https://use.typekit.net/pyi0msa.css');
    if(is_home()) {
        wp_enqueue_style('t-layout', $tplDirUri . '/css/t-layout.css');
        wp_enqueue_style('home', $tplDirUri . '/css/home.css');
    } elseif(is_page(getLoginPagesSlugs())) {
        wp_enqueue_style('t-login-webfont', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i');
        wp_enqueue_style('t-login', $tplDirUri . '/css/t-login.css');
    } else {
        wp_enqueue_style('t-icons', $tplDirUri . '/css/t-icons.css');
        // All audience pages
        wp_enqueue_style('t-layout', $tplDirUri . '/css/t-layout.css');
        wp_enqueue_style('t-form', $tplDirUri . '/css/t-form.css');
        // Widgets
        wp_enqueue_style('t-widgets', $tplDirUri . '/css/t-widgets.css');
        // Single
        wp_enqueue_style('t-single', $tplDirUri . '/css/t-single.css');
        wp_enqueue_style('t-datatables', $tplDirUri . '/css/t-datatables.css');
        // Page
        wp_enqueue_style('t-page', $tplDirUri . '/css/t-page.css');
        // Home
        wp_enqueue_style('t-home', $tplDirUri . '/css/t-home.css');
        // Author page
        wp_enqueue_style('t-author', $tplDirUri . '/css/t-author.css');
    }
}

/* Css for AMP */
function insertAmpCss() {
    require_once(get_template_directory() . '/css/article-next-links.css');
}

/* Scripts */
function insertScripts() {
	$tplDirUri = get_template_directory_uri();
    // Moving jquery to bottom
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', includes_url('/js/jquery/jquery.js'), array(), false, true);
    // Login page is different, it has its own scripts
    if(!is_page(getLoginPagesSlugs())) {
        // Add menu
        wp_enqueue_script('menu', $tplDirUri . '/js/menu.js', array('jquery'), false, true);
        // Add search
        wp_enqueue_script('search', $tplDirUri . '/js/search.js', array('jquery'), false, true);
        // Adding script to share links
        wp_enqueue_script('nv-share', $tplDirUri . '/lib/client/nv-share/nv-share.js', array(), false, true);
        if(is_home()) {
            // Add scroller
            wp_enqueue_script('scroller', $tplDirUri . '/js/scroller.js', array(), false, true);
            wp_enqueue_script('nv-slider', $tplDirUri . '/lib/client/nv-slider/nv-slider.js', array('scroller'), false, true);
            // Add newsletter
            wp_enqueue_script('nv-newsletter', $tplDirUri . '/js/newsletter.js', array('jquery'), false, true);
        }
        // Article script
        if(is_single()) {
            wp_enqueue_script('article-js', $tplDirUri . '/js/article.js', array('jquery'), false, true);
        }
        // Adding script to allow load more items in posts lists
        if(is_category() || is_archive() || is_search()) {
            wp_enqueue_script('loadMore', $tplDirUri . '/js/loadmore.js', array('jquery'), false, true);
        }
    }
}

/* Excerpt length */
function filterExcerpt($length) {
    return 20;
}

/* Excerpt "[...]" */
function filterExcerptMore($more) {
    return '...';
}

/* Load post teasers through ajax */
function getAjaxPosts() {
    $postsPerPage = isset($_POST['postsPerPage']) ? max(1, $_POST['postsPerPage']) : 4;
    $page = isset($_POST['idPage']) ? $_POST['idPage'] : 'home';
    $section = isset($_POST['idSection']) ? $_POST['idSection'] : 'home-featured';
    $offset = isset($_POST['offset']) ? max(0, $_POST['offset']) : 0;
    if(isset($_POST['extra'])) {
        $extra = $_POST['extra'];
        $postsParamExtra = is_array($extra) ? $extra : json_decode($extra);
    } else {
        $postsParamExtra = array();
    }

    $posts = ContentProxy::getPosts($page, $section, $postsPerPage, $offset, $postsParamExtra);
    $wquery = $posts['wpQuery'];
    if($wquery->have_posts()) {
        $GLOBALS['wp_query'] = $wquery;
        ob_start();
        while($wquery->have_posts()) {
            $wquery->the_post();
            articleTeaser(true, true, true);
        }
        $html = ob_get_clean();
    } else {
        $html = '';
    }
    $nextOffset = $offset + $postsPerPage;
    echo json_encode(array(
        'html' => $html,
        'page' => $page,
        'section' => $section,
        'offset' => $nextOffset < $wquery->found_posts ? $nextOffset : -1,      // If there're no more pages, offset is -1
        'extra' => $postsParamExtra
    ));
    die();
}

//filtra as mídias, se for admin pode ver todas, caso contrário, somente as imagens do autor
add_action('pre_get_posts', function ($wp_query) {
    global $current_user;
    if (\is_admin() && !\current_user_can('edit_others_posts')) {
        $wp_query->set('author', $current_user->ID);
    }
});

//Get treding news from Metrics
function get_tredding_news(){
	$url_metrics =  WP_SCRITPS_URL.'/trending-news.php';
	$response = wp_remote_get( $url_metrics);
	$result = array();
	if ( is_wp_error( $response ) ) {
   		$error_message = $response->get_error_message();
   		error_log('Erro ao tentar buscar os posts do metrics' . $error_message);
	} else { 		
		$result = json_decode($response['body']);		
	}

	return $result;
}

//Desabilitando o js 'wp-embed.min' o mesmo não é necessário
function deregister_scritp_wp_embed(){
	wp_deregister_script('wp-embed');
}

function navve_remove_from_admin_bar($wp_admin_bar) {
    if (!isAdminUser()) {
        $wp_admin_bar->remove_node('w3tc');
        $wp_admin_bar->remove_node('wpseo-menu');
    }
}
add_action('admin_bar_menu', 'navve_remove_from_admin_bar', 99999999);

function remove_menus() {
	if(!isAdminUser()){
		remove_menu_page( 'amp-options' );
		//remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
		remove_submenu_page('pp-calendar', 'edit.php?post_type=psppnotif_workflow');
		remove_submenu_page('options-general.php', 'table-of-contents'); 
	}
    if(isManagerUser()) {
        remove_menu_page('wpseo_amp');
        remove_menu_page('insert-php-code-snippet-manage');
        remove_menu_page('w3tc_dashboard');
        remove_menu_page('onddagpack-settings');
        remove_menu_page('options-general.php');
        remove_menu_page('theme-my-login');
        remove_menu_page('wp-user-avatar');
        remove_menu_page('sucuriscan');
        remove_menu_page('wp-mail-smtp');
        //remove_menu_page('tools.php');
        //remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
        remove_submenu_page('tools.php', 'remove_personal_data');
        remove_submenu_page('tools.php', 'export_personal_data');
        remove_submenu_page('tools.php', 'velvet-blues-update-urls.php');
        remove_submenu_page('tools.php', 'export.php');
        remove_submenu_page('tools.php', 'import.php');
        remove_submenu_page('options-general.php', 'privacy.php');
        remove_submenu_page('options-general.php', 'options-general.php');
        remove_submenu_page('options-general.php', 'options-writing.php');
        remove_submenu_page('options-general.php', 'amazon-s3-and-cloudfront');
        remove_submenu_page('options-general.php', '301options');
        remove_submenu_page('options-general.php', 'options-reading.php');
        remove_submenu_page('options-general.php', 'members-settings');
        remove_submenu_page('options-general.php', 'options-permalink.php');
        remove_submenu_page('options-general.php', 'gtm4wp-settings');
        remove_submenu_page('options-general.php', 'options-discussion.php');
        remove_submenu_page('options-general.php', 'options-media.php');       
    }

    if (!isAdminOrManagerUser()) {
        remove_menu_page('cmodsar_settings');
    }
}


function isAdminUser(){
	$user = wp_get_current_user();
	$allowed_roles = array('administrator');
	return ( array_intersect($allowed_roles, $user->roles ) );
}

function isManagerUser(){
    $user = wp_get_current_user();
    $allowed_roles = array('gerente');
    return ( array_intersect($allowed_roles, $user->roles ) );
}

function isAdminOrManagerUser(){
    $user = wp_get_current_user();
    $allowed_roles = array('administrator', 'gerente');
    return ( array_intersect($allowed_roles, $user->roles ) );
}

function changePublishPressPluginMenuLabel(){
	global $submenu;
	if(containsMenuPublishPress($submenu)){
		// TODO: tentar fazer funcionar o código que está comentado
		//O mesmo está funcioando, porém no cliente não é atualizado o label do menu
		$submenu['pp-calendar'][1][0]= "Pautas por Categoria";
		$submenu['pp-calendar'][1][3]= "Pautas por Categoria";
		
		/*$labelToChange = getLabelToChange($submenu);
		if($labelToChange){
			foreach($labelToChange as &$val){
				if($val == 'Histórico de orçamento'){
					$val = 'Pautas por Categoria';
				}
			}
		}
		*/		
	}
}

function getLabelToChange($submenu){
	$ppCalendar = $submenu["pp-calendar"];
	$postionOfMenu = getIndexItem($ppCalendar);
	
	if($postionOfMenu)
		return $ppCalendar[$postionOfMenu];
		
	return null;
}

function getIndexItem($ppCalendar){
	$index = false;
	foreach ($ppCalendar as $key=>$arrItems) {	
		if (in_array("pp-content-overview", $arrItems)) {
			$index = $key;
			break;
		}
	}
	return $index;
}

function containsMenuPublishPress($submenu){
	return array_key_exists('pp-calendar',$submenu);
}

function rewrite_user_profile(){
        global $wp_rewrite;
        $wp_rewrite->author_base = 'usuario';
        $wp_rewrite->author_structure = '/'.$wp_rewrite->author_base.'/%author%';
}

function max_entries_per_sitemap() {
    return 5000;
}

\add_action('after_body_open_tag', function () {
    if (!\function_exists('\gtm4wp_the_gtm_tag')) {
        return;
    }
    echo "\n".'<!-- 150309 - SMc gTM -->'."\n";
    \gtm4wp_the_gtm_tag();
    echo "\n".'<!-- 150309 - END SMc gTM -->'."\n";
}, 0);

function getBrands() {
    global $post;
    return \wp_get_post_terms($post->ID, 'brand');
}

function loadTaxonomies() {
        if(is_single()){
                $brands = getBrands();
                if(!empty($brands) && !is_wp_error($brands)){
                        add_filter('gtm4wp_compile_datalayer',function($datalayer) use ($brands) {
                                $datalayer['pageBrand'] = $brands[0]->slug;
                        	return $datalayer;
                	});
        	}
		 add_filter('gtm4wp_compile_datalayer',function($datalayer) use ($brands) {
                                $datalayer['currentPage'] = "noticias";
                        return $datalayer;
                });
                $subjects = getTermsByTaxonomy('subject');
                
                if(!empty($subjects)) sendToDataLayer(formatTerms($subjects),'pageSubject');
    }
}

function formatTerms($terms){
        $data = array();
        foreach($terms as $term){
                array_push($data, $term->slug);
        }
        return $data;
}

function dataLayerReamp() {

    if(is_home()){
        $event = 'dlNavve';
        $userId = 'undefined'; // id undefined until we get the google id.
        $pagePath = 'undefined';//get_page_uri();
        $url = get_site_url();
        $pageType = get_post_type();

        if(!empty($event)) sendToDataLayer($event,'event');
        if(!empty($userId)) sendToDataLayer($userId,'userId');
        if(!empty($pagePath)) sendToDataLayer($pagePath,'pagePath');
        if(!empty($url)) sendToDataLayer($url,'url');
        if(!empty($pageType)) sendToDataLayer($pageType,'pageType');
    }

    if(is_single()){
        $event = 'dlNavve';
        $postId = get_the_ID();
        $userId = 'undefined'; // id undefined until we get the google id.
        $authorId = get_post_field( 'post_author', $postId );
        $authorProfile = 'https://www.euqueroinvestir.com'.'/usuario/'.get_the_author_meta('user_nicename',$authorId);
        $authorName =  md5(get_the_author_meta('display_name',$authorId));
        $articleTitle = get_the_title();
        $articleDate = get_the_date("d/m/y");
        $articleTime = get_the_time("H:i");
        $articleKeywords = 'undefined';
        //$articleFinanceFormNameCustumer = $_POST['name'];
        //$articleFinanceFormEmailCustumer = $_POST['email'];
        //$articleFinanceFormFoneCustumer = $_POST['cell-number'];        
        //$articleHighlightedPerson = formatTerms(getTermsByTaxonomy('athlete'),'articleHighlightedPerson');
        //$articleEvent = formatTerms(getTermsByTaxonomy('competition'), 'articleEvent');
        //$articleCompany = formatTerms(getTermsByTaxonomy('team'), 'articleCompany');
        //$articleBrand = getBrands();
        $pagePath = get_page_uri();
        $url = get_permalink();
        $pageType = get_post_type();
        $pageSubType = 'single-' . get_post_type();
        //$pageAdStamp = getCampaignId(get_the_ID());

        if(!empty($pageAdStamp)) {
			sendToDataLayer($pageAdStamp,'pageAdStamp');
        }else {
        	$pageAdStamp = 'undefined';
        } 

        /*if(!empty($articleBrand) && !is_wp_error($articleBrand)){
            add_filter('gtm4wp_compile_datalayer',function($datalayer) use ($articleBrand) {
                $datalayer['articleBrand'] = $articleBrand[0]->slug;
                return $datalayer;
            });
        }*/

        $_post_tags = get_the_tags();
        if ( $_post_tags ) {
            $articleAttributes = array();
            foreach ( $_post_tags as $_one_tag ) {
                $articleAttributes[] = $_one_tag->slug;
            }
        }else{
            $articleAttributes = 'undefined';
        }


        if(!empty($event)) sendToDataLayer($event,'event');
        if(!empty($postId)) sendToDataLayer($postId,'postId');
        if(!empty($userId)) sendToDataLayer($userId,'userId');
        if(!empty($authorProfile)) sendToDataLayer($authorProfile,'authorProfile');
        if(!empty($authorId)) sendToDataLayer($authorId,'authorId');
        if(!empty($authorName)) sendToDataLayer($authorName,'authorName');
        if(!empty($articleTitle)) sendToDataLayer($articleTitle,'articleTitle');
        if(!empty($articleDate)) sendToDataLayer($articleDate,'articleDate');
        if(!empty($articleTime)) sendToDataLayer($articleTime,'articleTime');
        if(!empty($articleKeywords)) sendToDataLayer($articleKeywords,'articleKeywords');
        if(!empty($articleAttributes)) sendToDataLayer($articleAttributes,'articleAttributes');
        if(!empty($articleHighlightedPerson)) sendToDataLayer($articleHighlightedPerson,'articleHighlightedPerson');
        if(!empty($articleEvent)) sendToDataLayer($articleEvent,'articleEvent');
        if(!empty($articleCompany)) sendToDataLayer($articleCompany,'articleCompany');
        //if(!empty($articleBrand)) sendToDataLayer($articleBrand,'articleBrand');
        if(!empty($pagePath)) sendToDataLayer($pagePath,'pagePath');
        if(!empty($url)) sendToDataLayer($url,'url');
        if(!empty($pageType)) sendToDataLayer($pageType,'pageType');
        if(!empty($pageSubType)) sendToDataLayer($pageType,'pageSubType');
    }

    if(is_category()){
        $event = 'dlNavve';
        $userId = 'undefined'; // id undefined until we get the google id.
        $pagePath = $_SERVER['REQUEST_URI'];
        $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $pageType = get_post_type();
        $pageSubType = 'single-' . get_post_type();
		$pageAdStamp =  '';


        if(!empty($event)) sendToDataLayer($event,'event');
        if(!empty($userId)) sendToDataLayer($userId,'userId');
        if(!empty($pagePath)) sendToDataLayer($pagePath,'pagePath');
        if(!empty($url)) sendToDataLayer($url,'url');
        if(!empty($pageType)) sendToDataLayer($pageType,'pageType');
        if(!empty($pageSubType)) sendToDataLayer($pageType,'pageSubType');
    }

    if(is_tax('brand')){
        if(!empty($event)) sendToDataLayer($event,'event');        
    }
        $productInterestHome = 'undefined';
        $productInterestLead = 'undefined';
        $effectiveDate = 'undefined';
        $subscriptionPackage = 'undefined';
        $currencyCode = 'undefined';
        $subscriptionPackageFee = 'undefined';
        $subscriberCount = 'undefined';
        $purchaseDateTime = 'undefined';
        $purchaseTimeZone = 'undefined';
        $affiliateName = 'undefined';
        $region = 'undefined';
        $platformType = 'undefined';
        $deviceType = 'undefined';
        $transactionType = 'undefined';
        $PAID = 'undefined';
        $assetName = 'undefined';
        $customerID = 'undefined';
        $deviceID = 'undefined';
        $purchasePrice = 'undefined';
        $transactionID = 'undefined';
        $purchasePointLocation = 'undefined';
        $postalCode = 'undefined';
        $testAcc = 'undefined';
        $viewingDateTime = 'undefined';
        $viewingTimeZone = 'undefined';
        $duration = 'undefined';
        $startIndex = 'undefined';
        $stopIndex = 'undefined';
        $adjustedPrice = 'undefined';
        $adjustmentReason = 'undefined';

        if(!empty($productInterestHome)) sendToDataLayer($productInterestHome,'productInterestHome');
        if(!empty($productInterestLead)) sendToDataLayer($productInterestLead,'productInterestLead');
        if(!empty($effectiveDate)) sendToDataLayer($effectiveDate,'effectiveDate');
        if(!empty($subscriptionPackage)) sendToDataLayer($subscriptionPackage,'subscriptionPackage');
        if(!empty($currencyCode)) sendToDataLayer($currencyCode,'currencyCode');
        if(!empty($subscriptionPackageFee)) sendToDataLayer($subscriptionPackageFee,'subscriptionPackageFee');
        if(!empty($subscriberCount)) sendToDataLayer($subscriberCount,'subscriberCount');
        if(!empty($purchaseDateTime)) sendToDataLayer($purchaseDateTime,'purchaseDateTime');
        if(!empty($purchaseTimeZone)) sendToDataLayer($purchaseTimeZone,'purchaseTimeZone');
        if(!empty($affiliateName)) sendToDataLayer($affiliateName,'affiliateName');
        if(!empty($region)) sendToDataLayer($region,'region');
        if(!empty($platformType)) sendToDataLayer($platformType,'platformType');
        if(!empty($deviceType)) sendToDataLayer($deviceType,'deviceType');
        if(!empty($purchasePrice)) sendToDataLayer($purchasePrice,'purchasePrice');
        if(!empty($transactionID)) sendToDataLayer($transactionID,'transactionID');
        if(!empty($purchasePointLocation)) sendToDataLayer($purchasePointLocation,'purchasePointLocation');
        if(!empty($postalCode)) sendToDataLayer($postalCode,'postalCode');
        if(!empty($testAcc)) sendToDataLayer($testAcc,'testAcc');
        if(!empty($viewingDateTime)) sendToDataLayer($viewingDateTime,'viewingDateTime');
        if(!empty($viewingTimeZone)) sendToDataLayer($viewingTimeZone,'viewingTimeZone');
        if(!empty($duration)) sendToDataLayer($duration,'duration');
        if(!empty($startIndex)) sendToDataLayer($startIndex,'startIndex');
        if(!empty($stopIndex)) sendToDataLayer($stopIndex,'stopIndex');
        if(!empty($adjustedPrice)) sendToDataLayer($adjustedPrice,'adjustedPrice');
        if(!empty($adjustmentReason)) sendToDataLayer($adjustmentReason,'adjustmentReason');
}

function sendToDataLayer($data,$key){
	 add_filter('gtm4wp_compile_datalayer',function($datalayer) use ($data,$key) {
            $datalayer[$key] = $data;
            if (array_key_exists('pagePostAuthor',$datalayer)) {
                unset($datalayer['pagePostAuthor']);
            }/*
            if(array_key_exists('pageAttributes',$datalayer)){
                unset($datalayer['pageAttributes']);
            }*/
            /*if(array_key_exists('pageBrand',$datalayer)){
                unset($datalayer['pageBrand']);
            }*/
            if(array_key_exists('pagePostType',$datalayer)){
                unset($datalayer['pagePostType']);
            }
            if(array_key_exists('pagePostType2',$datalayer)){
                unset($datalayer['pagePostType2']);
            }
            if(array_key_exists('pagePostAuthorID',$datalayer)){
               unset($datalayer['pagePostAuthorID']);
            }
           return $datalayer;
        });
}

function getTermsByTaxonomy($taxonomy){
        global $post;
        $terms = \wp_get_post_terms($post->ID, $taxonomy);
        if(empty($terms))
                return NULL;

        return $terms;
}

function filterEmbeds($html) {
    if(preg_match('/\ssrc=[\'"]?http(s?):\/\/([^\s]+\.)?(youtube\.com|youtu\.be)\//', $html)) {
        $newHtml = '<div class="nv-embed nv-embed-youtube">' . $html . '</div>';
    } else {
        $newHtml = $html;
    }
    return $newHtml;
}

add_filter('the_content', function( $content ){
    //--Remove all inline styles--
    $content = preg_replace('/ style=("|\')(.*?)("|\')/','',$content);
    //$content = preg_replace( '/(width|height)="\d*"/', '', $content);
    return $content;
}, 20);


function update_contact_methods( $contactmethods ) {

    unset( $contactmethods['aim'] );
    unset( $contactmethods['jabber'] );
    unset( $contactmethods['yim'] );
    unset( $contactmethods['myspace'] );
    unset( $contactmethods['soundcloud'] );    
    unset( $contactmethods['pinterest'] );
    unset( $contactmethods['youtube'] );
    unset( $contactmethods['wikipedia'] );
    unset( $contactmethods['tumblr'] );

    return $contactmethods;

}
add_filter( 'user_contactmethods', 'update_contact_methods' );


function show_password_fields() {
    if (current_user_can('administrator')) {
        return true;
    }
    return false;
}

//add_filter('show_password_fields', 'show_password_fields');

//Filtro para aparecer todos os usuários ao selecionar o autor do post
add_filter( 'wp_dropdown_users_args', 'add_subscribers_to_dropdown', 10, 2 );
function add_subscribers_to_dropdown( $query_args, $r ) {
    $query_args['who'] = '';
    return $query_args;
}

/* Navve Newsletter */
function navveSignNewsletter()
{
    try {
        if (!is_email($_POST['email'])) {
            throw new Exception('Por favor, informe um endereço de e-mail válido!');
        }

        $data = array(
            "email" => $_POST['email'],
            "LastConversion__c" => "newsletter"
        );

        $result = sendNewLead($data);

        if ($result && $result->result === "success") {
            echo json_encode(array('status' => 'success', 'message' => 'Email cadastrado com sucesso, em breve entraremos em contato com você.'));
            wp_die();
        } else {
            throw new Exception('Por favor, tente mais tarde!');
        }
        
    }catch (Exception $e) {
        echo json_encode(array('status' => 'error', 'message' => $e->getMessage()));
        wp_die();
    }
}
add_action("wp_ajax_navve_newsletter_form", "navveSignNewsletter");
add_action("wp_ajax_nopriv_navve_newsletter_form", "navveSignNewsletter");
/* Fim Navve Newsletter */

/**
 * Return sizes for thumbnails.
 * @author Lanza
 * @return array[]
 */
function getThumbnailSizes() {
    return array(

        /* Thumb sizes used in home page */
        array('name' => 'nv-home-featured-small', 'width' => 360, 'height' => 217, 'crop' => true),
        array('name' => 'nv-home-featured-medium', 'width' => 168, 'height' => 130, 'crop' => true, 'sizes' => '(max-width: 1023px) 360px, 168px'),
        array('name' => 'nv-home-featured-big', 'width' => 301, 'height' => 231, 'crop' => true, 'sizes' => '(max-width: 1023px) 360px, 301px'),
        array('name' => 'nv-home-last-news-small', 'width' => 47, 'height' => 36, 'crop' => true),
        array('name' => 'nv-home-category-small', 'width' => 112, 'height' => 84, 'crop' => true),
        array('name' => 'nv-home-category-medium', 'width' => 153, 'height' => 96, 'crop' => true, 'sizes' => '(max-width: 1023px) 112px, 153px'),
        array('name' => 'nv-home-category-big', 'width' => 253, 'height' => 158, 'crop' => true, 'sizes' => '(max-width: 1023px) 112px, 253px'),
        array('name' => 'nv-home-sliders-small', 'width' => 248, 'height' => 155, 'crop' => true),

        /* Thumb sizes from previous version used in other pages */
        array('name' => 'nv-list-mobile', 'width' => 79, 'height' => 79, 'crop' => true),
        array('name' => 'nv-list', 'width' => 152, 'height' => 152, 'crop' => true, 'sizes' => '(max-width: 1023px) 79px, 152px'),
        array('name' => 'nv-article-mobile', 'width' => 362, 'height' => 0, 'crop' => false),
        array('name' => 'nv-article', 'width' => 895, 'height' => 0, 'crop' => false, 'sizes' => '(max-width: 1023px) 362px, 895px'),
        array('name' => 'nv-highlight-mobile', 'width' => 360, 'height' => 216, 'crop' => true),
        array('name' => 'nv-highlight', 'width' => 559, 'height' => 336, 'crop' => true, 'sizes' => '(max-width: 1023px) 360px, 559px')

    );
}

/* Set theme thumbnails and common features */
$themeFeatures = new ThemeFunctions(getThumbnailSizes());

/* Hooks */
add_action('init', 'rewrite_user_profile');
add_action('init', 'setupConstants');
add_action('admin_menu', 'remove_menus', 9999);
add_action('admin_menu', 'changePublishPressPluginMenuLabel', 99999);
add_action('after_setup_theme', 'setupFeatures', 50);
add_action('after_setup_theme', 'setupMenu', 50);
add_action('wp_enqueue_scripts', 'insertStylesheets', 50);
add_action('wp_enqueue_scripts', 'insertScripts', 50);
add_action('wp_enqueue_scripts', 'loadTaxonomies');
add_action('wp_enqueue_scripts', 'dataLayerReamp');
add_action('wp_ajax_loadmore', 'getAjaxPosts');
add_action('wp_ajax_nopriv_loadmore', 'getAjaxPosts');
add_action('wp_footer', 'deregister_scritp_wp_embed' );
add_action('amp_post_template_css', 'insertAmpCss');
add_filter('image_size_names_choose', 'imageSizesToChoose');
add_filter('excerpt_length', 'filterExcerpt', 999);
add_filter('excerpt_more', 'filterExcerptMore');
add_filter('wpseo_sitemap_entries_per_page', 'max_entries_per_sitemap');
add_filter('embed_oembed_html', 'filterEmbeds');
