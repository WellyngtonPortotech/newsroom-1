<?php
/**
 * Search Results page.
 */

require_once get_template_directory() . '/inc/blocks/article-teaser.php';
require_once get_template_directory() . '/inc/backend/content-proxy.php';

get_header();
?>
<div class="site-body site-search">
    <?php
    $posts = ContentProxy::getPosts('search', 'search-results', 25, 0, array('s' => get_search_query(false)));
    $wpQuery = $posts['wpQuery'];
    if($wpQuery->have_posts()) {
        ?>
        <section class="category-list search-results">
            <header class="search-results-header">
                <h1>Resultados da busca</h1>
            </header>
            <div class="category-list-body search-results-list">
                <?php
                while($wpQuery->have_posts()) {
                    $wpQuery->the_post();
                    articleTeaser(true, false, true);
                }
                wp_reset_postdata();
                ?>
            </div>
            <?php
            if($posts['pages'] > 1) {
                ?>
                <footer class="category-list-footer search-results-footer">
                    <p class="list-load-trigger" data-url="<?php echo htmlspecialchars(admin_url('admin-ajax.php'), ENT_QUOTES, 'UTF-8'); ?>"
                       data-id-page="search"
                       data-id-section="search-results"
                       data-posts-per-page="25"
                       data-offset="25"
                       data-extra="<?php echo '{"s":"' . get_search_query() . '"}'; ?>">
                        <i class="ic-plus"></i>
                        <i class="ic-spinner"></i>
                        <span class="list-load-text">Mais resultados</span>
                    </p>
                </footer>
                <?php
            }
            ?>
        </section>
        <?php
    } else {
        ?>
        <section class="search-no-results">
            <h1>Nada encontrado</h1>
            <p>Não há nada no site com o termo procurado.</p>
        </section>
        <?php
    }
    ?>
</div>
<?php get_footer(); ?>
