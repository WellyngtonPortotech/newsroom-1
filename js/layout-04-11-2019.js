/* Menu */
(function ($) {

    const stopPropagation = function (ev) {
        ev.stopPropagation();
    };

    const toggleMenu = function (ev) {
        stopPropagation(ev);
        const menu = $('.h-menu');
        const body = $('body');
        if(menu.hasClass('active')) {
            menu.removeClass('active');
            body.removeClass('site-menu-active');
        } else {
            menu.addClass('active');
            body.addClass('site-menu-active');
        }

    };

    const toggleSubmenu = function (ev) {
        ev.preventDefault();
        ev.stopPropagation();
        const menuItem = $(ev.currentTarget).parent();
        menuItem.toggleClass('open', !menuItem.hasClass('open')).siblings('.menu-item-has-children.open').removeClass('open');
    };

    const setup = function () {
        const menu = $('.h-menu');
        menu.find('.h-menu-content').on('click', stopPropagation);
        menu.find('.ic-bars, .ic-x, .h-menu-content a').not('.menu-item-has-children > a').on('click', toggleMenu);
        menu.find('.menu-item-has-children > a').on('click', toggleSubmenu);
        menu.on('click', toggleMenu);
    };

    $(setup);

})(jQuery);

/* Search */
(function ($) {

    const toogleSearch = function () {
        const searchPanel = $('.h-search');
        if(searchPanel.hasClass('active')) {
            searchPanel.removeClass('active');
        } else {
            searchPanel.addClass('active');
            searchPanel.find('input[type="search"]').focus();
        }
    };

    const setup = function () {
        $('.h-search .ic-search').on('click', toogleSearch);
    };

    $(setup);

})(jQuery);

/* Share */
(function ($) {

    const stopPropagation = function (ev) {
        ev.stopPropagation();
    };

    const hideSharePanel = function () {
        $('.t-share-panel').removeClass('active');
        $('body').off('click', hideSharePanel);
    };

    const showSharePanel = function (data) {
        let panel = $('.t-share-panel');
        const body = $('body');
        if(panel.length) {
            panel.addClass('active');
        } else {
            const url = encodeURIComponent(data.url || '');
            const title = encodeURIComponent(data.title || '');
            const text = encodeURIComponent(data.text || '');
            const hcode = [
                '<div class="t-share-panel"><div class="t-share-panel-content"><h1>Escolha como quer compartilhar</h1><ul>',
                '<li class="t-share-whatsapp"><a href="whatsapp://send?text=' + url + '" target="_blank"><i class="ic-whatsapp"></i> <span>Whatsapp</span></a>',
                '<li class="t-share-facebook"><a href="http://www.facebook.com/sharer.php?u=' + url + '" target="_blank"><i class="ic-facebook"></i> <span>Facebook</span></a>',
                '<li class="t-share-twitter"><a href="https://twitter.com/intent/tweet?original_referer=' + url + '&text=' + text + '&tw_p=tweetbutton&url=' + url + '" target="_blank"><i class="ic-twitter"></i> <span>Twitter</span></a>',
                '<li class="t-share-email"><a href="mailto:?subject=' + title + '&body=' + text + '  ' + url + '" target="_blank"><i class="ic-mail"></i> <span>E-mail</span></a>',
                '</ul></div></div>'
            ];
            panel = $(hcode.join(''));
            body.append(panel);
            panel.on('click', stopPropagation);
            setTimeout(() => {
                panel.addClass('active');
            }, 100);
        }
        body.off('click', hideSharePanel).on('click', hideSharePanel);
    };

    const startShare = function (ev) {
        stopPropagation(ev);
        const data = $(ev.currentTarget).data();
        if(navigator.share) {
            navigator.share({
                title: data.title,
                text: data.text,
                url: data.url
            });
        } else {
            showSharePanel(data);
        }
    };

    $('.share-trigger').on('click', startShare);

})(jQuery);

/* Category carousel */

(function ($) {

    let trackPositions = [];

    const updateTrack = function (xpos, container, body, track) {
        $('.category-list-bt-prev', container).toggleClass('nv-bt-visible', xpos < 0);
        $('.category-list-bt-next', container).toggleClass('nv-bt-visible', track.width() + xpos > body.outerWidth());
        track.css({
            transform: 'translate(' + xpos + 'px, 0)'
        });
    };

    const centerTrack = function (container, body, track) {
        const children = track.children('.article-teaser');
        if(children.length) {
            const bodyWidth = body.outerWidth();
            const trackWidth = track.width();
            let xpos = 0;
            // if(trackWidth > bodyWidth) {
                const cIndex = Math.max(Math.ceil(children.length / 2) - 1, 0);
                const child = $(children[cIndex]);
                xpos = 0 - child.position().left - (child.outerWidth(true) / 2) + (bodyWidth / 2);
            //}
            updateTrack(xpos, container, body, track);
        }
    };

    const getFirstElementOfNextPage = function (body, track) {
        const children = track.children('.article-teaser');
        const bodyRight = body[0].getBoundingClientRect().right;
        for(let a = 0, n = children.length; a < n; a++) {
            let el = children[a];
            if(el.getBoundingClientRect().right > bodyRight) {
                return el;
            }
        }
        return null;
    };

    const getFirstElementOfPrevPage = function (body, track) {
        const children = track.children('.article-teaser');
        for(let a = children.length - 1; a >= 0; a--) {
            let el = children[a];
            if(el.getBoundingClientRect().left < 0) {
                return el;
            }
        }
        return null;
    };

    const nextPage = function (container, body, track) {
        const itemEl = getFirstElementOfNextPage(body, track);
        if(itemEl) {
            const jitem = $(itemEl);
            const newX = Math.max(0 - jitem.position().left + (jitem.outerWidth(true) - jitem.width()), 0 - track.width() + body.outerWidth());
            updateTrack(newX, container, body, track);
        }
    };

    const prevPage = function (container, body, track) {
        const itemEl = getFirstElementOfPrevPage(body, track);
        if(itemEl) {
            const jitem = $(itemEl);
            const newX = Math.min(0, 0 - jitem.position().left + (body.outerWidth() - jitem.outerWidth(true)));
            updateTrack(newX, container, body, track);
        }
    };

    const init = function (index, container) {
        const jcontainer = $(container);
        const body = jcontainer.find('.category-list-body');
        const track = jcontainer.find('.category-list-track');
        centerTrack(jcontainer, body, track);
        jcontainer.find('.category-list-bt-next').off('click').on('click', nextPage.bind(this, jcontainer, body, track));
        jcontainer.find('.category-list-bt-prev').off('click').on('click', prevPage.bind(this, jcontainer, body, track));
    };

    const setup = function () {
        $('.category-list-scroller').each(init);
    };

    $(function () {
        let setupDelay;        
        setup();
        $(window).on('resize', () => {
            window.cancelAnimationFrame(setupDelay);
            setupDelay = window.requestAnimationFrame(setup);
        });
    });
