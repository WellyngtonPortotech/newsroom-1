<?php
defined('ABSPATH') or die('Not Allowed');

add_action('rss2_item', 'onddag_add_rss_image_node');
add_action('rss_item', 'onddag_add_rss_image_node');
function onddag_add_rss_image_node() {
    global $post;
        if(has_post_thumbnail($post->ID)) {
            $thumbnail = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            echo("\n<image>" . sanitize_url($thumbnail) . "</image>\n");
        } else {
			 echo("\n<image>" .  get_template_directory(). '/img/404.jpg' . "</image>\n");
		}

		(int) $id = $post->ID;
        if ( $id > 0 ) {
                $output = sprintf( '<site xmlns="com-wordpress:feed-additions:1">%d</site>', $id );
                echo $output;
        }
}

add_filter('wp_handle_upload_prefilter', 'onddag_custom_upload_filter' );
function onddag_custom_upload_filter( $file ){
    $name = $file['name'];
    $ext = pathinfo($name, PATHINFO_EXTENSION);
    $name = substr($name, 0, strpos($name, $ext));
    $file['name'] = sanitize_title($name) . '.' . $ext;
    return $file;
}