(function ($) {

    const toggleSearch = function () {
        $('.nv-search').toggleClass('active');
    };

    const closeSearch = function (evt) {
        evt.stopPropagation();
        evt.preventDefault();
        $('.nv-search').removeClass('active');
    }

    const init = function () {
        $('.nv-search .nv-ic-search').on('click', toggleSearch);
        $('.nv-search .nv-ic-close').on('click', closeSearch);
    };

    $(init);

})(jQuery);