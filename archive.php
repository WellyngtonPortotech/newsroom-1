<?php

require_once get_template_directory() . '/inc/blocks/article-teaser.php';
require_once get_template_directory() . '/inc/blocks/category-list.php';
require_once get_template_directory() . '/inc/backend/content-proxy.php';

get_header();

$tax_data['taxonomy'] = get_queried_object()->taxonomy;
$tax_data['slug'] = get_queried_object()->slug;
?>
<div class="site-body site-category">
    <div class="category-featured-box">
        <div class="category-featured">
            <h1 class="category-featured-title"><?php single_cat_title(); ?></h1>
            <?php
            // Featured articles
            $posts = ContentProxy::getPosts('archive', 'archive-featured', 4, 0, array('taxonomy' => $tax_data['taxonomy'], 'slug' => $tax_data['slug']));
            $wpQuery = $posts['wpQuery'];
            if($wpQuery->have_posts()) {
                $count = 0;
                while($wpQuery->have_posts()) {
                    $wpQuery->the_post();
                    articleTeaser(true, false, false, '', 'nv-highlight');
                    if($count == 1) {
                        ?>
                        <div class="ad-wrapper ad-home-300x250"><div class="ad-slot ad-300x250"><div id="div-gpt-ad-1562364384279-0"></div></div></div>
                        <?php
                    }
                    $count++;
                    }
                wp_reset_postdata();
            }
            ?>
        </div>
    </div>
    <div class="ad-wrapper ad-home-728x90"><div class="ad-slot ad-728x90" id="div-gpt-ad-1562364733729-0"></div></div>

    <div class="category-sections">
        <div class="category-more">
            <?php
            	categoryList(array('archive', 'archive-list-01', 6, 4, array('taxonomy' => $tax_data['taxonomy'], 'slug' => $tax_data['slug'])));
            ?>
        </div>
    </div>
</div>
<?php
get_footer();
