# Wordpress Newsroom Theme

Use this theme for news websites.

## Posts

Every list of posts in this theme is loaded through the **ContentProxy** class (currently located in _inc/backend_ folder). 
The theme calls the method `getPosts` with the following arguments:

1. _$idPage_ (string) Page identifier. Arbitrary name to identify the website's page.
1. _$idSection_ (string) Page's section identifier. Arbitrary name to identify a slot within the page.
1. _$postsPerPage_ (int) How many posts to show.
1. _$offset_ (int) Offset index.
1. _$extra_ (array) Extra parameters to use in specific queries. They should be appended in WP_Query's call.

Theme expects return of an array as following:

````
array(
    'title' => '',           /* Section title to show to user. */
    'posts' => array(),      /* All posts returned by the query. It should be deprecated in next versions. */
    'page' => 1,             /* Current page. */
    'pages' => 1,            /* Total of pages for current query. */
    'wpQuery' => WP_Query,   /* The WP_Query object that holds access to the posts. */
    'url' => ''              /* Url of section to show to user, when applicable. If not applicable, it must be an empty string. */
)
```` 

## Ads

All ad slots in theme are identified by the `nv-ad` css class. Some are only visible when site is in mobile mode, some when it's in desktop mode, one in both modes.

### In both modes

* Mobile Superbanner (1 slot, 300x100 pixels)

### In mobile mode

* Medium Rectangle (2 slots, 300x250 pixels)
* Smartphone Static Banner (1 slot, 300x50 pixels)

### In desktop mode

* Large Rectangle (1 slot, 336x280 pixels)
* Billboard (1 slot, 970x250 pixels)
* Super Leaderboard (1slot, 970x90 pixels)

## Menus

Theme has support for 5 menus:

1. *Main menu*
1. *Headers CTAs*, to show call to action buttons in site's header.
1. *Footer menu* for links to show only in site's footer. Useful for links as "privacy policy", "terms of use"...
1. *Social menu*, to show links for client's profiles in social networks.
1. *Group sites menu*, to show links for client's other sites. 

## Questions

Ask [Lanza](mailto:fabio.lanzarin@portotech.org).