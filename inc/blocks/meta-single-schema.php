<?php
if(!is_single()) {
    return '';
}

$postID = get_the_ID();
$thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($postID), 'full');
?>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "mainEntityOfPage": {
            "@type": "NewsArticle",
            "@id": "<?php echo esc_url(get_permalink($postID))?>"
        },
        "headline": "<?php echo get_the_title($postID);?>",



        "image": [
            "<?php echo esc_url($thumbnail_src[0]);?>"
        ],



        "datePublished": "<?php echo date(DATE_W3C, get_the_time('U', $postID));?>",
        "dateModified": "<?php echo the_modified_date('c', '', '', false);?>",
        "author": {
            "@type": "Person",
            "name": "<?php echo esc_attr(get_the_author());?>"
        },
        "publisher": {
            "@type": "Organization",
            "name": "Torcedores",
            "logo": {
                "@type": "ImageObject",
                "url": "<?php echo esc_url(get_template_directory_uri() . '/img/favicon/favicon-260x260.png'); ?>"
            }
        }
    }
</script>
<?php
$taxonomies = getTaxonomiesFromPost();
if(empty($taxonomies))
        return;

foreach($taxonomies as $row => $innerArray){
        foreach($innerArray as $innerRow => $value){
?>
               <meta property="article:<?php  echo $innerRow; ?>" content="<?php echo $value ;?>">
<?php
        }
}

function getTaxonomiesFromPost(){
        $data = array();
        if(defined('CUSTOM_TAXONOMIES')){
		$customTaxonomies = unserialize(CUSTOM_TAXONOMIES);
                foreach($customTaxonomies as $taxonomy){
                        $terms = get_the_terms($postID,$taxonomy);
                        if(!is_wp_error($terms) && is_array($terms) && count($terms)>0){
                                foreach($terms as $term){
                                        array_push($data,array($taxonomy=>$term->name));
                                }
                        }
                }
        }
        return $data;
}
?>

