<?php
function javascript_variables() {
?>
    <script type="text/javascript">
        var ajax_url = '<?php echo admin_url("admin-ajax.php");?>';
        //var ajax_nonce = '<?php //echo wp_create_nonce("secure_nonce_name");?>';
    </script>
<?php
}

/** 
 * Clean all special characters from string.
 *
 * @param string $content Text to be cleaned
 *
 * @return string $content Cleaned text
 *
 * @author Wellyngton Lourenço <wellyngton.lourenco@portotech.org>
 */
function nvClearText($content) {
    $content = strtolower($content);
    $content = preg_replace('/[áàãâä]/ui', 'a', $content);
    $content = preg_replace('/[éèêë]/ui', 'e', $content);
    $content = preg_replace('/[íìîï]/ui', 'i', $content);
    $content = preg_replace('/[óòõôö]/ui', 'o', $content);
    $content = preg_replace('/[úùûü]/ui', 'u', $content);
    $content = preg_replace('/[ç]/ui', 'c', $content);
    $content = preg_replace('/[^a-z0-9]/i', '-', $content);
    return $content;
}

function nvSendContactForm() {
	//verificação do request
	//não estava sendo utilizado.
	//wp_verify_nonce('secure-nonce-name', 'security');
	try {
		if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['cell-number'])) {
			throw new Exception('Por favor verique se todos os dados foram preenchidos corretamente.');
		}
		if (!is_email($_POST['email'])) {
			throw new Exception('Por favor, informe um endereço de e-mail válido!');
		}
		$data = array(
			"name" => $_POST['name'],
			"email" => $_POST['email'],
			"telefone" => $_POST['cell-number'],
			"titulo-da-publicacao" => $_POST['titulo-da-publicacao'],
			"url-da-publicacao" => $_POST['url-da-publicacao'],
			//"traffic_source" => "direct",
        	//"traffic_medium" => "Site",
			"lp_liquidez" => $_POST['value-investment'],
			//"colaborador" => $_POST['name-contributor'],
			//"email_colaborador" => $_POST['email-contributor'],
			"identificador" => "euqueroinvestir-form"
		);

		if (!isset($_POST['traffic_source'])) {
            $data["traffic_source"] = "direct";
        }

        if (!isset($_POST['traffic_medium'])) {
            $data["traffic_medium"] = "Site";
        }

        if (isset($_POST['utm_source'])) {
        	$data["utm_source"] = $_POST['utm_source'];
        }else{
			$data["utm_source"] = "euqueroinvestir.com";
		}

        if (isset($_POST['utm_medium'])) {
            $data["utm_medium"] = $_POST['utm_medium'];
        }else{
			$data["utm_medium"] = "formulario";
		}

        if (isset($_POST['utm_campaign'])) {
            $data["utm_campaign"] = $_POST['utm_campaign'];
        }else{
			$data["utm_campaign"] = $_POST['url-da-publicacao'];
		}

        if (isset($_POST['utm_term'])) {
            $data["utm_term"] = $_POST['utm_term'];
        }

        if (isset($_POST['utm_anuncio'])) {
            $data["utm_anuncio"] = $_POST['utm_anuncio'];
        }

        if (isset($_POST['utm_grupo_de_anuncios'])) {
            $data["utm_grupo_de_anuncios"] = $_POST['utm_grupo_de_anuncios'];
        }

        if (isset($_POST['gclid'])) {
            $data["gclid"] = $_POST['gclid'];
        }

        if (isset($_POST['utm_keyword'])) {
            $data["utm_keyword"] = $_POST['utm_keyword'];
        }

        if (isset($_POST['utm_tipo_de_keyword'])) {
            $data["utm_tipo_de_keyword"] = $_POST['utm_tipo_de_keyword'];
        }

        if (isset($_POST['utm_origem'])) {
            $data["utm_origem"] = $_POST['utm_origem'];
        }

        if (isset($_POST['utm_tipo_de_aparelho'])) {
            $data["utm_tipo_de_aparelho"] = $_POST['utm_tipo_de_aparelho'];
        }

        if (isset($_POST['utm_campanha'])) {
            $data["utm_campanha"] = $_POST['utm_campanha'];
        }

		if (isset($_POST['utm_content'])) {
            $data["utm_content"] = $_POST['utm_content'];
        }

       	//neste if, verificar se o núm começa com +1
        if (substr($_POST['cell-number'], 0, 2) === '+1') {
        	echo json_encode(array('status' => 'success', 'message' => 'Dados enviados com sucesso, em breve entraremos em contato com você.'));
			wp_die();
        }else{

        	$result = sendNewLead($data);
	
			if ($result) {
				echo json_encode(array('status' => 'success', 'message' => 'Dados enviados com sucesso, em breve entraremos em contato com você.'));
				wp_die();
			} else {
				throw new Exception('Por favor, tente mais tarde!'.$result);
			}


        	/*$result = sendToRdstation($data);
			if ($result && $result->result === "success") {
				echo json_encode(array('status' => 'success', 'message' => 'Dados enviados com sucesso, em breve entraremos em contato com você.'));
				wp_die();
			} else {
				throw new Exception('Por favor, tente mais tarde!');
			}*/



        }
	}
	catch (Exception $e) {
		echo json_encode(array('status' => 'error', 'message' => $e->getMessage()));
		wp_die();
	}
}

/**
 * @deprecated
 * function is not in use, It was replaced by lead funciton
 * @return $this
 */
function sendToRdstation($data) {
	if (!($urlAPI = URL_API_RDS_EQI15)) {
		return false;
	}
	$data["token_rdstation"] = "5cc4f3b1f45fd4b60afc99b6d26c38be";
	$args = array(
		'headers' => array(
			'Content-Type' => 'application/json ; charset=utf-8'
		),
		'body' => json_encode($data)
	);
	
	$response = wp_remote_post(esc_url_raw($urlAPI), $args);
	$response_code = wp_remote_retrieve_response_code($response);
	$response_body = wp_remote_retrieve_body($response);
	if (!in_array($response_code, array(200,201)) || is_wp_error($response_body))
		return false;
	
	return json_decode($response_body);
}


/**
 * Returns oauth2 refresh token data
 * get refresh token data 
 * @return array
 */
function getToken(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://euqueroinvestir.my.salesforce.com/services/oauth2/token?grant_type=refresh_token&client_id=3MVG9LBJLApeX_PBu6.ebCc1efHeWzCZUa3R3h2mBJj.Jb14Klvl4YdGpyChQS9ogRbdDIYT_0308kgw3rY8V&client_secret=798E2CA2B6F940C7F1F3FED3E7848FCB0D4905E20FB9E1A84E707CFC866D88AF&refresh_token=5Aep8615B7Psrq3qbkq..8jOXl_uYBhLiwD86cnZF1mYMubp9OERYF40R8QrqUYeFgKomBtzrpFiSt9beJzfxZ8',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Cookie:BrowserId=1tp4NE35EeyXv-EFro3zkQ; CookieConsentPolicy=0:1; LSKey-c$CookieConsentPolicy=0:1'
      ),
    ));


    try {
      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      if ($httpcode == 200) {
        return json_decode($response,true);
      }
      else {
        echo 'Unexpected HTTP status: ' . $httpcode . ' ' .curl_error($curl);
      }
    }
    catch(Exception $e) {
      echo 'Error: ' . $e->getMessage();
    }

    curl_close($curl);
    
}

/**
 * Returns output of new lead
 * @param array | $oauth get data for refresh token getToken()
 * @param string | $authorization
 * @param string | $httpcode
 * @return array
 */
function sendNewLead($data){

	try {
		$oauth = getToken();
	} catch (Exception $e) {
		echo 'Get token error: ',  $e->getMessage(), "\n";
	}

	$curl = curl_init();
	
	$fullName  = explode(' ',$data['name']);
	$firstName = $fullName[0];
	$lastName  = $fullName[1];

	$lastConversion = ($data['LastConversion__c'] ? $data['LastConversion__c'] : nvClearText($data['titulo-da-publicacao']));
    
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://euqueroinvestir.my.salesforce.com/services/data/v51.0/sobjects/Lead',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS =>'{
		"FirstName": "'.$firstName.'",
		"LastName": "'.($lastName ? $lastName : $data['email']).'",
		"Email": "'.$data['email'].'",
		"MobilePhone": "'.$data['telefone'].'",
		"RecordTypeId": "0126g0000003xsWAAQ",
		"LeadSource": "'.$data['identificador'].'",
		"RDValueRangeReported__c": "'.$data['lp_liquidez'].'",
		"HaveInvestments__c": "",
		"LastConversion__c": "'.$lastConversion.'" ,
		"InteractionURL__c": "'.$data['url-da-publicacao'].'",
		"UTMSource__c": "'.$data['utm_source'].'",
		"UTMCampaing__c": "'.$data['utm_campaign'].'",
		"UTMContent__c": "'.$data['utm_content'].'" ,
		"UTMMedium__c": "'.$data['utm_medium'].'",
		"UTMTerm__c": "'.$data['utm_term'].'",
		"GCLID__c": "'.$data['gclid'].'"
	}',
	  CURLOPT_HTTPHEADER => array(
		'Authorization: Bearer '.$oauth['access_token'],
		'Content-Type: application/json',
		'Cookie: BrowserId=1tp4NE35EeyXv-EFro3zkQ; CookieConsentPolicy=0:1; LSKey-c$CookieConsentPolicy=0:1'
	  ),
	));
	
	try {
	  $response = curl_exec($curl);
	  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	  if ($httpcode == 201) {
		$res =  json_decode($response,true);
		return $res['success'];
	  }
	  else {
		echo 'Unexpected HTTP status: ' . $httpcode . ' ' .curl_error($curl);
	  }
	}
	catch(Exception $e) {
	  echo 'Error: ' . $e->getMessage();
	}
	curl_close($curl);
}


add_action("wp_head", "javascript_variables");
add_action("wp_ajax_finance_advice_send", "nvSendContactForm");
add_action("wp_ajax_nopriv_finance_advice_send", "nvSendContactForm");

