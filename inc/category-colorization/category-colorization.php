<?php

add_action('create_category', 'generate_colors_hsl', 10, 2);
function generate_colors_hsl($term_id, $taxonomy_term_id){
	$meta_key = "color_hue";
	$saturation = 74;
	$lightness = 41;
	$hue = get_current_hue_value();
	
	//hue = (n + 30)
	$hue = $hue+30;
	add_term_meta ($term_id, $meta_key, $hue);

}


function get_current_hue_value(){
	global $wpdb;
	$result = $wpdb->get_results('SELECT meta_value from wp_termmeta where meta_key = "color_hue" order by meta_id desc limit 1');

	return (count($result)>0)? $result[0]->meta_value: 0;

}

function print_category_color($term_id){
	$attribut_style = '';
	$term_meta_single =  get_term_meta($term_id);
	if(count($term_meta_single)) {
		$color_hsl = 'background-color: hsl('.$term_meta_single["color_hue"][0].', 74%, 41%);';
		$attribut_style= 'style="'.$color_hsl.'"';
	}
	
	echo $attribut_style;
	
}


