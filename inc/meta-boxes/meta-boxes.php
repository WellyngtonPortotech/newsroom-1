<?php
function isValidUserRole(){
	$user = wp_get_current_user();
	$allowed_roles = array('administrator','gerente', 'editor');
	return ( array_intersect($allowed_roles, $user->roles ) );
}

function remove_meta_boxes_side(){
    remove_meta_box('categorydiv', 'post', 'side');
	
}

function remove_meta_box_bottom(){
	if(!isAdminUser()){
		//remove_meta_box('wpw_auto_poster_meta', 'post', 'normal');	
		remove_meta_box('instant_article_meta_box', 'post', 'normal');	
	}
} 

function yoasttobottom() {
	return 'low';
}

function custom_meta_boxes($post_type, $post){
    add_meta_box('categorydiv', 'Categorias', 'meta_box_category', 'post', 'side', 'high');
}

function meta_box_category( $post, $box ) {
	$defaults = array( 'taxonomy' => 'category' );
	if ( ! isset( $box['args'] ) || ! is_array( $box['args'] ) ) {
		$args = array();
	} else {
		$args = $box['args'];
	}
	$r = wp_parse_args( $args, $defaults );
	$tax_name = esc_attr( $r['taxonomy'] );
	$taxonomy = get_taxonomy( $r['taxonomy'] );
	?>
	<div id="taxonomy-<?php echo $tax_name; ?>" class="categorydiv">
		<ul id="<?php echo $tax_name; ?>-tabs" class="category-tabs">
			<li class="tabs"><a href="#<?php echo $tax_name; ?>-all"><?php echo $taxonomy->labels->all_items; ?></a></li>
			<li class="hide-if-no-js"><a href="#<?php echo $tax_name; ?>-pop"><?php echo esc_html( $taxonomy->labels->most_used ); ?></a></li>
		</ul>

		<div id="<?php echo $tax_name; ?>-pop" class="tabs-panel" style="display: none;">
			<ul id="<?php echo $tax_name; ?>checklist-pop" class="categorychecklist form-no-clear" >
				<?php
					$categoriesIds  =get_categories_by_slugs(array('destaque','extra', 'novidades'));
					if (!isValidUserRole()) {

						$popular_ids = wp_popular_terms_checklist($tax_name, 0, 10, false);
						$popular_ids = array_diff($popular_ids, $categoriesIds);
						build_most_used($popular_ids, $tax_name);
					} else {
						$popular_ids = wp_popular_terms_checklist($tax_name);
					} 
				?>
			</ul>
		</div>

		<div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
			<?php
			$name = ( $tax_name == 'category' ) ? 'post_category' : 'tax_input[' . $tax_name . ']';
			echo "<input type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
			?>
			<ul id="<?php echo $tax_name; ?>checklist" data-wp-lists="list:<?php echo $tax_name; ?>" class="categorychecklist form-no-clear">
				<?php
					if (isValidUserRole()) {
						wp_terms_checklist($post->ID, array('taxonomy' => $tax_name, 'popular_cats' => $popular_ids));

					} else {
						$result = wp_terms_checklist($post->ID, array('taxonomy' => $tax_name, 'popular_cats' => $popular_ids, 'echo' => false));
						$result = mb_convert_encoding($result, 'html-entities', 'utf-8');
						$doc = new DOMDocument();
						$doc->loadHTML($result);
						$xpath = new DOMXPath($doc);
						foreach($categoriesIds as $categoryId){
						 	$elements = $xpath->query('//li[@id = "category-'.$categoryId.'"]');
                                                	$node = $elements->item(0);
                                                	$node->parentNode->removeChild($node);
						}
						echo $doc->saveHTML($doc->documentElement);
					} 

				?>
			</ul>
		</div>
	<?php if ( current_user_can( $taxonomy->cap->edit_terms ) ) : ?>
			<div id="<?php echo $tax_name; ?>-adder" class="wp-hidden-children">
				<a id="<?php echo $tax_name; ?>-add-toggle" href="#<?php echo $tax_name; ?>-add" class="hide-if-no-js taxonomy-add-new">
					<?php
						/* translators: %s: add new taxonomy label */
						printf( __( '+ %s' ), $taxonomy->labels->add_new_item );
					?>
				</a>
				<p id="<?php echo $tax_name; ?>-add" class="category-add wp-hidden-child">
					<label class="screen-reader-text" for="new<?php echo $tax_name; ?>"><?php echo $taxonomy->labels->add_new_item; ?></label>
					<input type="text" name="new<?php echo $tax_name; ?>" id="new<?php echo $tax_name; ?>" class="form-required form-input-tip" value="<?php echo esc_attr( $taxonomy->labels->new_item_name ); ?>" aria-required="true"/>
					<label class="screen-reader-text" for="new<?php echo $tax_name; ?>_parent">
						<?php echo $taxonomy->labels->parent_item_colon; ?>
					</label>
					<?php
					$parent_dropdown_args = array(
						'taxonomy'         => $tax_name,
						'hide_empty'       => 0,
						'name'             => 'new' . $tax_name . '_parent',
						'orderby'          => 'name',
						'hierarchical'     => 1,
						'show_option_none' => '&mdash; ' . $taxonomy->labels->parent_item . ' &mdash;',
					);

					/**
					 * Filters the arguments for the taxonomy parent dropdown on the Post Edit page.
					 *
					 * @since 4.4.0
					 *
					 * @param array $parent_dropdown_args {
					 *     Optional. Array of arguments to generate parent dropdown.
					 *
					 *     @type string   $taxonomy         Name of the taxonomy to retrieve.
					 *     @type bool     $hide_if_empty    True to skip generating markup if no
					 *                                      categories are found. Default 0.
					 *     @type string   $name             Value for the 'name' attribute
					 *                                      of the select element.
					 *                                      Default "new{$tax_name}_parent".
					 *     @type string   $orderby          Which column to use for ordering
					 *                                      terms. Default 'name'.
					 *     @type bool|int $hierarchical     Whether to traverse the taxonomy
					 *                                      hierarchy. Default 1.
					 *     @type string   $show_option_none Text to display for the "none" option.
					 *                                      Default "&mdash; {$parent} &mdash;",
					 *                                      where `$parent` is 'parent_item'
					 *                                      taxonomy label.
					 * }
					 */
					$parent_dropdown_args = apply_filters( 'post_edit_category_parent_dropdown_args', $parent_dropdown_args );

					wp_dropdown_categories( $parent_dropdown_args );
					?>
					<input type="button" id="<?php echo $tax_name; ?>-add-submit" data-wp-lists="add:<?php echo $tax_name; ?>checklist:<?php echo $tax_name; ?>-add" class="button category-add-submit" value="<?php echo esc_attr( $taxonomy->labels->add_new_item ); ?>" />
					<?php wp_nonce_field( 'add-' . $tax_name, '_ajax_nonce-add-' . $tax_name, false ); ?>
					<span id="<?php echo $tax_name; ?>-ajax-response"></span>
				</p>
			</div>
		<?php endif; ?>
	</div>
	<?php
}

function build_most_used($popular_ids, $taxonomy)
{
    $post = get_post();

    if ($post && $post->ID) {
        $checked_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
    } else {
        $checked_terms = array();
    }
    
    $categoriesIds  =get_categories_by_slugs(array('destaque','extra', 'novidades'));

    $terms = get_terms($taxonomy, array('orderby' => 'count', 'order' => 'DESC', 'number' => 10, 'hierarchical' => false, 'exclude' => $categoriesIds));
    $tax = get_taxonomy($taxonomy);

    $popular_ids = array();
    foreach ((array) $terms as $term) {
        $id = "popular-$taxonomy-$term->term_id";
        $checked = in_array($term->term_id, $checked_terms) ? 'checked="checked"' : ''; ?>

    <li id="<?php echo $id; ?>" class="popular-category">
      <label class="selectit">
        <input id="in-<?php echo $id; ?>" type="checkbox" <?php echo $checked; ?> value="<?php echo (int) $term->term_id; ?>" <?php disabled(!current_user_can($tax->cap->assign_terms)); ?> />
<?php
    /** This filter is documented in wp-includes/category-template.php */
    echo esc_html(apply_filters('the_category', $term->name)); ?>
    </label>
  </li>

  <?php

    }
}

function get_categories_by_slugs($slugs = array()){
	$categoryIds = array();
	foreach($slugs as $slug){
		$category = get_category_by_slug($slug);
		if($category)
			array_push($categoryIds,$category->term_id);
	}
	return $categoryIds;
}
add_action('admin_menu', 'remove_meta_boxes_side',9999);
add_action('add_meta_boxes', 'custom_meta_boxes', 2, 9999);
add_action('admin_head','remove_meta_box_bottom');
add_filter( 'wpseo_metabox_prio', 'yoasttobottom',9999);

?>
