(function ($) {

    const error = function (form, message) {
        form.find('.nv-input-error').text(message);
        form.removeClass('nv-success nv-loading').addClass('nv-error');
    };

    const success = function (form) {
        form.removeClass('nv-error nv-loading').addClass('nv-success');
    };

    const loading = function (form) {
        form.removeClass('nv-success nv-error').addClass('nv-loading');
    };

    const submit = function (evt) {
        evt.preventDefault();
        const form = $(evt.currentTarget);
        if(!form.hasClass('nv-loading')) {
            loading(form);
            const userEmail = form.find('.nv-input-email').val();
            if(/^[^@\s]+@[^@\s]+$/.test(userEmail)) {
                $.ajax({
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'navve_newsletter_form',
                        email: userEmail
                    },
                    dataType:"json",
                    success: () => {
                        success(form);
                    },
                    error: () => {
                        error(form, 'Ocorreu um erro. Tente novamente mais tarde.');
                    }
                });
            } else {
                error(form, 'Por favor informe um e-mail válido.');
            }
        }
    };

    $(() => {
        $('.nv-newsletter-form').on('submit', submit);
    });

})(jQuery);