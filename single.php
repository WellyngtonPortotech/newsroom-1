<?php
get_header();
if (have_posts()) : while (have_posts()) : the_post();
    ?>
    <div class="site-body site-article">
        <div class="content-block">
            <div id="post-data-attr" data-post-id="<?php the_ID(); ?>" data-author-id="<?php the_author_meta('ID'); ?>"></div>
            <article class="article<?php if (has_post_thumbnail()) echo ' article-with-thumbnail'; ?>"
                     itemprop="mainEntity" itemscope itemtype="http://schema.org/NewsArticle"
                     itemid="<?php the_permalink(); ?>">
                <div class="article-cols">
                    <div class="article-cols-main">
                        <header class="content-pad">
                            <h1 class="article-headline" itemprop="headline"><?php the_title(); ?></h1>
                            <p class="article-author">
                                <span itemprop="author" itemscope itemtype="http://schema.org/Person">
                                    Por
                                    <span class="author-avatar"><?php echo wp_kses_post(get_avatar(get_the_author_meta('email'), '40', '', get_the_author_meta('display_name'), array('extra_attr' => 'itemprop="image"'))); ?></span>
                                    <a href="<?php echo esc_url('/usuario/'.get_the_author_meta('user_nicename')); ?>"><?php the_author(); ?><b class="author-name" itemprop="name"></b></a>
                                </span>
                                <span class="article-date" itemprop="datePublished"
                                      content="<?php echo date(DATE_W3C, get_the_time('U', get_the_ID())); ?>">
                                    <span class="article-date-before">em</span>
                                    <span class="article-date-time"><?php echo esc_html(get_the_time("H:i")); ?></span>
                                    de
                                    <span class="article-date-day"><?php echo esc_html(get_the_date("d/m/y")); ?></span>
                                </span>
                            </p>
                            <div class="author-description">
                                <dl>
                                    <dt class="author-description-name no-display"><?php the_author(); ?></dt>
                                    <dd class="author-description-text"><?php echo empty(get_the_author_meta('description')) ? 'Colaborador do Torcedores' : get_the_author_meta('description'); ?></dd>
                                    <?php
                                    $authorTwitter = get_the_author_meta('twitter');
                                    if ($authorTwitter) {
                                        ?>
                                        <dt class="author-description-links-label no-display">Links</dt>
                                        <dd class="author-description-links-values">
                                            <a href="https://www.twitter.com/<?php echo $authorTwitter; ?>"
                                               target="_blank">
                                                <i class="ic-twitter"></i>
                                                <span class="no-display">Twitter</span>
                                            </a>
                                        </dd>
                                        <?php
                                    }
                                    ?>
                                </dl>
                            </div>
                            <?php
                            $categories = get_the_category();
                            if (count($categories)) {
                                $cat = $categories[0];
                                if (count($categories) > 1) {
                                    $cat = $categories[1];
                                }
                                ?>
                                <p class="article-category">
                                    <span class="no-display">Categoria:</span>
                                    <a class="article-category-label"
                                       itemprop="genre" <?php print_category_color($cat->term_id); ?>
                                       href="<?php echo esc_url(get_category_link($cat->term_id)); ?>">
                                        <!-- TODO: Add background-color for category. -->
                                        <?php echo $cat->cat_name; ?>
                                    </a>
                                </p>
                                <?php
                            }
                            ?>
                            <div class="share article-share">
                                <p class="share-trigger" data-title="<?php esc_html(the_title()); ?>"
                                   data-text="Veja essa matéria em <?php echo esc_attr(get_bloginfo('name')); ?>" data-url="<?php the_permalink(); ?>">
                                    <span>Compartilhe</span> <i class="ic-share"></i>
                                </p>
                            </div>
                        </header>
                        <?php
                        // Featured image
                        if (has_post_thumbnail()) {
                            $thumbId = get_post_thumbnail_id(get_the_ID());
                            $thumbInfo = get_post($thumbId);
                            $image = wp_get_attachment_image_src($thumbId, 'large');
                            $alt = get_post_meta($thumbId, '_wp_attachment_image_alt', true);
                            $attachmentTitle = get_the_title($thumbId);
                            ?>
                            <div class="article-featured-photo" style="background-image: url(<?php echo $image[0]; ?>)">
                                <figure>
                                    <img src="<?php echo $image[0]; ?>"
                                         srcset="<?php echo wp_get_attachment_image_srcset($thumbId, 'nv-article'); ?>"
                                         sizes="<?php echo wp_get_attachment_image_sizes($thumbId, 'nv-article'); ?>"
                                         alt="<?php echo esc_attr($alt || ''); ?>"
                                         title="<?php echo esc_attr($attachmentTitle || ''); ?>"/>
                                    <?php
                                    $captxt = '';
                                    if (strlen(str_replace(' ', '', $thumbInfo->post_content))) {
                                        $captxt = 'Foto: ' . esc_html($thumbInfo->post_content);
                                    }
                                    if (strlen(str_replace(' ', '', $thumbInfo->post_excerpt))) {
                                        $captxt = 'Crédito: ' . esc_html($thumbInfo->post_excerpt);
                                    }
                                    if (!empty($captxt)) {
                                        ?>
                                        <figcaption>
                                            <p><?php echo esc_html($captxt); ?></p>
                                        </figcaption>
                                        <?php
                                    }
                                    ?>
                                </figure>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="article-body-content" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                        <aside class="article-aside">
                            <?php
                            //insere o content recommendation
                            if (in_array('navve-content-recommendation/navve-content-recommendation.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                                $ret = navveContentRecommendationPosts(get_the_ID());
                                echo $ret;
                            }

                            $isMobile = wp_is_mobile();
                            ?>
                        </aside>
                    </div>
                    <div class="article-cols-aside">
                    <?php 
                        $upperSideAd = '
                            <div class="ad-wrapper">
                                <div id="div-gpt-ad-1593012840331-0" class="ad-slot ad-slot-col-right-01 ad-300x250"></div>
                            </div>
                        ';
                        $sideAds = '
                            <div class="ad-wrapper">
                                <div id="div-gpt-ad-1593012840331-0" class="ad-slot ad-slot-col-right-01 ad-300x250"></div>
                            </div>
                            <div class="ad-wrapper sticky">
                                <div id="div-gpt-ad-1593012892906-0" class="ad-slot ad-slot-col-right-02 ad-300x600"></div>
                            </div>
                        ';
                        //Check if the below side AD will be replaced
                        if (in_array('navve-auto-cta-inserter/navve-auto-cta-inserter.php', apply_filters('active_plugins', get_option('active_plugins')))){
                            global $post, $wp_query;

                            if ( $post === NULL ) {
                                return;
                            }

                            if ( !is_object( $post ) ) {
                                $post = $wp_query->post;
                            }

                            $sideCta = navveGetAvailableCtas( $post, true );

                            if($sideCta && !empty( $sideCta )){
                                $showCta = $sideCta[array_rand( $sideCta )];
                                echo "<div class='nv-desktop-only'>" . $upperSideAd . htmlspecialchars_decode( $showCta->CONTENT ) . "</div>";
                            }else {
                                echo $sideAds;
                            }
                        }else {
                            echo $sideAds;
                        }
                    ?>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <?php
endwhile; endif;
get_footer();
