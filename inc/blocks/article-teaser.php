<?php

/**
 * Prints an article teaser block from the WP query in context.
 * @author Lanza
 * @param bool $showThumb Whether to show thumbnail image
 * @param bool $showCategory Whether to show category label
 * @param bool $showExcerpt Whether to show post's excerpt
 * @param string $class CSS class to apply to the block
 * @param string $thumbnailSize Name of thumbnail size (as defined in functions.php)
 * @param int $titleLevel Level of article title. Default is 2. Level 2 = tag <h2>
 */
function articleTeaser($showThumb = true, $showCategory = true, $showExcerpt = false, $class = '', $thumbnailSize= 'nv-list', $titleLevel = 2) {
    $titleTag = 'h' . $titleLevel;
    ?>
    <article class="article-teaser<?php if($class) echo " $class"; ?>" itemscope itemtype="http://schema.org/NewsArticle">
        <<?php echo $titleTag; ?> class="article-teaser-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $titleTag; ?>>
        <p class="article-teaser-author">
            <a href="<?php the_permalink(); ?>">
                Por <span class="author-name" itemprop="author"><?php the_author(); ?></span>
                <span class="article-date-before">em</span>
                <span class="article-date-day" itemprop="datePublished"
                      content="<?php echo date(DATE_W3C, get_the_time('U', get_the_ID())); ?>">
                    <?php echo esc_html(get_the_date("d/m/y")); ?>
                </span>
            </a>
        </p>
        <?php
        $categories = get_the_category();
        if($showCategory && count($categories)) {
            $cat = null;
            if (count($categories) > 1) {
                foreach ($categories as $category) {
                    if (!in_array($category->slug, array("destaque", "novidades", "extra"))) {
                        $cat = $category;
                        break;
                    }
                }
                if (!$cat) $cat = $categories[1];
            } else {
                $cat = $categories[0];
            }
            ?>
            <p class="article-category">
                <span class="no-display">Categoria:</span>
                <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>">
                    <span class="article-category-label" <?php print_category_color($cat->term_id); ?> itemprop="genre"><?php echo esc_html($cat->name); ?></span>
                </a>
            </p>
            <?php
        }
        if($showThumb) {
            $thumbId = get_post_thumbnail_id(get_the_ID());
            ?>
            <p class="article-teaser-photo">
                <a href="<?php the_permalink(); ?>">
                    <?php
                    if(has_post_thumbnail()) {
                        the_post_thumbnail($thumbnailSize, array(
                            'title' => get_the_title($thumbId)  ,
                            'alt' => get_post_meta($thumbId, '_wp_attachment_image_alt', true) || '',
                            'itemprop' => 'image'
                        ));
                    } else {
                        ?>
                        <img src="<?php echo get_template_directory_uri() . '/img/teaser-default.png'; ?>" alt=""/>
                        <?php
                    }
                    ?>
                </a>
            </p>
            <?php
        }
        if($showExcerpt) {
            ?>
            <p class="article-teaser-excerpt">
                <a href="<?php the_permalink(); ?>">
                    <?php echo wp_strip_all_tags(get_the_excerpt()); ?>
                </a>
            </p>
            <?php
        }
        ?>
        <footer class="share article-teaser-share">
            <p class="share-trigger" data-title="<?php esc_html(the_title()); ?>"
                data-text="Veja essa matéria em <?php echo esc_attr(get_bloginfo('name')); ?>" data-url="<?php the_permalink(); ?>">
                <i class="ic-share"></i>
            </p>
        </footer>
    </article>
    <?php
}
