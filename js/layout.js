/* Menu */
(function ($) {

    const stopPropagation = function (ev) {
        ev.stopPropagation();
    };

    const toggleMenu = function (ev) {
        stopPropagation(ev);
        const menu = $('.h-menu');
        const body = $('body');
        if(menu.hasClass('active')) {
            menu.removeClass('active');
            body.removeClass('site-menu-active');
        } else {
            menu.addClass('active');
            body.addClass('site-menu-active');
        }

    };

    const toggleSubmenu = function (ev) {
        ev.preventDefault();
        ev.stopPropagation();
        const menuItem = $(ev.currentTarget).parent();
        menuItem.toggleClass('open', !menuItem.hasClass('open')).siblings('.menu-item-has-children.open').removeClass('open');
    };

    const setup = function () {
        const menu = $('.h-menu');
        menu.find('.h-menu-content').on('click', stopPropagation);
        menu.find('.ic-bars, .ic-x, .h-menu-content a').not('.menu-item-has-children > a').on('click', toggleMenu);
        menu.find('.menu-item-has-children > a').on('click', toggleSubmenu);
        menu.on('click', toggleMenu);
    };

    $(setup);

})(jQuery);

/* Search */
(function ($) {

    const toogleSearch = function () {
        const searchPanel = $('.h-search');
        if(searchPanel.hasClass('active')) {
            searchPanel.removeClass('active');
        } else {
            searchPanel.addClass('active');
            searchPanel.find('input[type="search"]').focus();
        }
    };

    const setup = function () {
        $('.h-search .ic-search').on('click', toogleSearch);
    };

    $(setup);

})(jQuery);

/* Category carousel */
(function ($) {

    const updateTrack = function (xpos, container, body, track) {
        $('.category-list-bt-prev', container).toggleClass('nv-bt-visible', xpos < 0);
        $('.category-list-bt-next', container).toggleClass('nv-bt-visible', track.width() + xpos > body.outerWidth());
        track.css({
            transform: 'translate(' + xpos + 'px, 0)'
        });
    };

    const centerTrack = function (container, body, track) {
        const children = track.children('.article-teaser');
        if(children.length) {
            const bodyWidth = body.outerWidth();
            const trackWidth = track.width();
            let xpos = 0;
            // if(trackWidth > bodyWidth) {
                const cIndex = Math.max(Math.ceil(children.length / 2) - 1, 0);
                const child = $(children[cIndex]);
                xpos = 0 - child.position().left - (child.outerWidth(true) / 2) + (bodyWidth / 2);
            //}
            updateTrack(xpos, container, body, track);
        }
    };

    const getFirstElementOfNextPage = function (body, track) {
        const children = track.children('.article-teaser');
        const bodyRight = body[0].getBoundingClientRect().right;
        for(let a = 0, n = children.length; a < n; a++) {
            let el = children[a];
            if(el.getBoundingClientRect().right > bodyRight) {
                return el;
            }
        }
        return null;
    };

    const getFirstElementOfPrevPage = function (body, track) {
        const children = track.children('.article-teaser');
        for(let a = children.length - 1; a >= 0; a--) {
            let el = children[a];
            if(el.getBoundingClientRect().left < 0) {
                return el;
            }
        }
        return null;
    };

    const getLeftLimitPos = function (body, track) {
        return 0 - track.width() + body.outerWidth();
    };

    const nextPage = function (container, body, track) {
        const itemEl = getFirstElementOfNextPage(body, track);
        if(itemEl) {
            const jitem = $(itemEl);
            const newX = Math.max(0 - jitem.position().left + (jitem.outerWidth(true) - jitem.width()), getLeftLimitPos(body, track));
            updateTrack(newX, container, body, track);
        }
    };

    const prevPage = function (container, body, track) {
        const itemEl = getFirstElementOfPrevPage(body, track);
        if(itemEl) {
            const jitem = $(itemEl);
            const newX = Math.min(0, 0 - jitem.position().left + (body.outerWidth() - jitem.outerWidth(true)));
            updateTrack(newX, container, body, track);
        }
    };

    const init = function (index, container) {
        const jcontainer = $(container);
        const body = jcontainer.find('.category-list-body');
        const track = jcontainer.find('.category-list-track');
        centerTrack(jcontainer, body, track);
        jcontainer.find('.category-list-bt-next').off('click').on('click', nextPage.bind(this, jcontainer, body, track));
        jcontainer.find('.category-list-bt-prev').off('click').on('click', prevPage.bind(this, jcontainer, body, track));
        // Enable swipe
        let xstart = 0, xend = 0;
        track.on('touchstart mousedown', ev => {
            xstart = ev.type == 'touchstart' ? ev.originalEvent.changedTouches[0].screenX : ev.screenX;
        }).on('touchend mouseup', ev => {
            xend = ev.type == 'touchend' ? ev.originalEvent.changedTouches[0].screenX : ev.screenX;
            if(xend < xstart) {
                nextPage(container, body, track);
            } else if(xend > xstart) {
                prevPage(container, body, track);
            }
        });
    }; 
    
    const setup = function () {
        $('.category-list-scroller').each(init);
    };

    $(function () {
        let setupDelay;
        setup();
        $(window).on('resize', () => {
            window.cancelAnimationFrame(setupDelay);
            setupDelay = window.requestAnimationFrame(setup);
        });
    });

})(jQuery);
