<?php

class ContentProxy {

    /**
     * Returns a WP_Query object with the executed query.
     * @param $queryParams Parameters of WP_Query.
     * @return WP_Query
     */
    private static function executeQuery($queryParams) {
        return new WP_Query($queryParams);
    }

    /**
     * Gets the posts to show in a specific section of a page.
     * @param $idPage string Page identifier.
     * @param $idSection string Page's section identifier.
     * @param $postsPerPage integer How many posts to show.
     * @param $offset integer Offset index.
     * @param $extra array Extra parameters to use in specific queries.
     * @return array('title' => string, 'posts' => array, 'page' => integer, 'pages' => integer, 'wpQuery' => WP_Query, 'url' => string)
     */
    public static function getPosts($idPage, $idSection, $postsPerPage = 5, $offset = 0, $extra = array()) {
        // Default values
        $result = array(
            'title' => '',
            'posts' => array(),
            'page' => max(1, floor($offset / $postsPerPage) + 1),
            'pages' => 1,
            'wpQuery' => null,
            'url' => ''     /* Url of category or tag page. */
        );

        // Perform the query according to idPage and idSection
        switch($idPage) {
            case 'home':
                switch($idSection) {
                    case 'home-featured':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'destaque'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = "";
                        break;
                    case 'home-featured-01':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'home-featured-01'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        break;
                    case 'home-featured-02':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'home-featured-02'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        break;
                    case 'home-featured-03':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'home-featured-03'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        break;
                    case 'home-featured-04':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'home-featured-04'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        break;
                    case 'home-top-five':
                        if (in_array('navve-trend-posts/navve-trend-posts.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                            $trendPosts = navveTppSelectPosts();
                            if ($trendPosts) {
                                $trendPostsId = array();
                                foreach ($trendPosts as $value) {
                                    array_push($trendPostsId, $value['POST_ID']);
                                }
                                $queryParams = array(
                                    'post__in' => $trendPostsId
                                );
                            }else{
                                $queryParams = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => $postsPerPage,
                                    'post_status' => 'publish',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'category',
                                            'field' => 'slug',
                                            'terms' => 'destaque'
                                        )
                                    )
                                );
                            }
                            $postsQuery = self::executeQuery($queryParams);
                            $result['title'] = 'Mais lidas';
                            $result['posts'] = $postsQuery->posts;
                            $result['wpQuery'] = $postsQuery;
                        }else{
                            $queryParams = array(
                                'post_type' => 'post',
                                'posts_per_page' => $postsPerPage,
                                'post_status' => 'publish',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'category',
                                        'field' => 'slug',
                                        'terms' => 'destaque'
                                    )
                                )
                            );
                            $postsQuery = self::executeQuery($queryParams);
                            $result['title'] = 'Mais lidas';
                            $result['posts'] = $postsQuery->posts;
                            $result['wpQuery'] = $postsQuery;
                        }
                        break;
                    case 'home-last-news':
                        $categoryTitle = "Últimas notícias";
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'orderby' => 'date',
                            'order'   => 'DESC',
                            'suppress_filters' => true
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = $categoryTitle;
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('noticias', 'category');
                        break;
                    case 'home-category-01':
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $postsPerPage,
                            'paged' => 1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'economia'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = 'Economia';
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('economia', 'subject');
                        break;
                    case 'home-category-02':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'mercados'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Mercados";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('mercados','subject');
                        break;
                    case 'home-category-03':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'acoes'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Ações";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('acoes','subject');
                        break;
                    case 'home-category-special':
                        $categoryTitle = 'Especiais';
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'especiais'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = $categoryTitle;
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('especiais','category');
                        break;
                    case 'home-category-04':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'negocios'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Negócios";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('negocios','subject');
                        break;
                    case 'home-category-05':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'fundos-investimentos'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Fundos de Investimentos";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('fundos-investimentos','subject');
                        break;
                    case 'home-category-06':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'fundos-imobiliarios'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Fundos Imobiliários";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('fundos-imobiliarios','subject');
                        break;
                    case 'home-category-columnists':
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => 'colunistas'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = 'Colunistas';
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('colunistas','category');
                        break;
                    case 'home-category-07':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'renda-fixa'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Renda Fixa";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('renda-fixa','subject');
                        break;
                    case 'home-category-08':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'poupanca'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Poupança";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('poupanca','subject');
                        break;
                    case 'home-category-09':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'planos-previdencia'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Planos de Previdência";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('planos-previdencia','subject');
                        break;
                    case 'home-category-10':
                        $queryParams = array(
                            'post_type' => 'post',
                            'posts_per_page' => $postsPerPage,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'subject',
                                    'field' => 'slug',
                                    'terms' => 'tesouro-direto'
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = "Tesouro Direto";
                        $result['posts'] = $postsQuery->posts;
                        $result['wpQuery'] = $postsQuery;
                        $result['url'] = get_term_link('tesouro-direto','subject');
                        break;
                }
                break;
            case 'category':
                switch($idSection) {
                    case 'category-featured':
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'tax_query' => array(
                                array(
                                    'taxonomy' => $extra['taxonomy'],
                                    'field' => 'slug',
                                    'terms' => $extra['slug']
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        break;
                    case 'category-list-01':
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'offset' => $offset,
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'tax_query' => array(
                                array(
                                    'field' => 'slug',
                                    'taxonomy' => $extra['taxonomy'],
                                    'terms' => $extra['slug']
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = 'Mais notícias';
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        break;
                }
                break;
            case 'archive':
                switch($idSection) {
                    case 'archive-featured':
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'tax_query' => array(
                                array(
                                    'field' => 'slug',
                                    'taxonomy' => $extra['taxonomy'],
                                    'terms' => $extra['slug']
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        break;
                    case 'archive-list-01':
                        $queryParams = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'offset' => $offset,
                            'posts_per_page' => $postsPerPage,
                            'paged' => $result['page'],
                            'tax_query' => array(
                                array(
                                    'field' => 'slug',
                                    'taxonomy' => $extra['taxonomy'],
                                    'terms' => $extra['slug']
                                )
                            )
                        );
                        $postsQuery = self::executeQuery($queryParams);
                        $result['title'] = 'Mais notícias';
                        $result['posts'] = $postsQuery->posts;
                        $result['pages'] = $postsQuery->max_num_pages;
                        $result['wpQuery'] = $postsQuery;
                        break;
                }
                break;
            case 'author':
                // Use values from $offset and $extra to build the query.
                $queryParams = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => $postsPerPage,
                    'paged' => $result['page'],
                    'author__in' => array($extra['author']),
                    'orderby' => 'post_date',
                    'order' => 'DESC'
                );
                $postsQuery = self::executeQuery($queryParams);
                $result['title'] = 'Notícias';
                $result['posts'] = $postsQuery->posts;
                $result['pages'] = $postsQuery->max_num_pages;
                $result['wpQuery'] = $postsQuery;
                break;
            case 'search':
                $queryParams = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => $postsPerPage,
                    'paged' => $result['page'],
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    's' => $extra['s']
                );
                $postsQuery = self::executeQuery($queryParams);
                $result['title'] = 'Resultados da busca';
                $result['posts'] = $postsQuery->posts;
                $result['pages'] = $postsQuery->max_num_pages;
                $result['wpQuery'] = $postsQuery;
                break;
            case 'article':
                $queryParams = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'post__not_in' => array($extra['post_id']),
                    'posts_per_page' => $postsPerPage,
                    'tax_query' => array(
                        array(
                            'field' => 'slug',
                            'taxonomy' => $extra['taxonomy'],
                            'terms' => $extra['slug']
                        )
                    )
                );
                $postsQuery = self::executeQuery($queryParams);
                $result['title'] = 'Leia mais';
                $result['posts'] = $postsQuery->posts;
                $result['pages'] = $postsQuery->max_num_pages;
                $result['wpQuery'] = $postsQuery;
                break;
        }
        return $result;
    }

}
