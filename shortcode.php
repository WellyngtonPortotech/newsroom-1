<?php
add_shortcode('DAZN', 'createWidgetDazn');
add_shortcode('DAZN_VIDEO', 'createWidgetDaznVideo');
add_shortcode('TESTE-DE-PERFIL', 'createProfileTest');
add_shortcode('FORM-RD-STATION', 'createFormRD');

function createWidgetDazn(){

	$iframe = '<script id="daznwidget" src="https://secure.spox.com/daznpic/daznwidget.js?c=brtorcedores"></script>';

	if (is_amp_endpoint() ) {
		$iframe = '';//'<iframe src="https://secure.spox.com/daznpic/daznscheduleptbr.html?c=brtorcedores&amp;tm=5194227" width="100%" height="148px" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" data-mce-fragment="1"></iframe>';
	}

	return $iframe;
}

function createWidgetDaznVideo($atts){
	$idVideo = $atts['video_id'];
	if(empty($idVideo)) return '';

	$iframe = '<script src="//player.performgroup.com/eplayer.js#42244d43d79de6e0f4ae8d1421.oct3w2oxqrq4184s2m0d0p58h$videoid='.$idVideo.'" async></script>';
	
	if(is_amp_endpoint()){
		$iframe = '<amp-video-iframe width="480" height="270" poster="" layout="responsive" src="https://player.performgroup.com/eplayer/ep4/assets/amp-iframe-loader.html?ep4on#42244d43d79de6e0f4ae8d1421.oct3w2oxqrq4184s2m0d0p58h$videoid='.$idVideo.'"><div overflow tabindex=0 role=button aria-label="Show Player">Show Player</div></amp-video-iframe>';
	}

	return $iframe;
}

function createProfileTest(){
	$tplDirUri = get_template_directory_uri();
	wp_enqueue_style('t-shortcodes', $tplDirUri . '/css/t-shortcodes.css');
	$snippet = '
	<div class="container-test-profile">
		<div class="block-test">
			<p>
				Uma das formas mais eficientes de identificarmos o nosso perfil de investidor, é realizando um teste de perfil.
			</p>
			<p>Você já fez seu teste de perfil? Descubra qual seu perfil de investidor! 
				<a href="https://materiais.euqueroinvestir.com/teste-de-perfil-de-investidor/" rel="nofollow"  target=”_blank” class="shortc-button small orange navve-tracker-clk-profile-tester">
					<span class="fa " aria-hidden="true">
					</span>
					Teste de Perfil
				</a>
			</p>
		</div>
	</div>';
	return $snippet;

}

function createFormRD() {

	$form = '<section class="contact-finance-advice-form">
            <h2 class="contact-finance-advice-form-title no-display">Fale com um assessor</h2>
	    <h2>Faça um diagnóstico de seus atuais investimentos com um Assessor certificado e imerso no mercado financeiro.</h2>
	    <p>Nossa equipe faz o mapeamento de seu perfil e agenda com você a conversa com um Assessor de Investimentos.</p>
            <form  id="finance_advice_send" method="post" action="">
                <div class="control-group">
                    <label class="control-label">Seu nome</label>
                    <input class="form-field" name="name" type="text"/>
                    <span class="field-message" name="name-error" id="name-error"></span>
                </div>
                <div class="control-group">
                    <label class="control-label">Seu e-mail</label>
                    <input class="form-field" name="email"  type="email"/>
                    <span class="field-message" name="email-error" id="email-error"></span>
                </div>
                <div class="control-group">
                    <label class="control-label">Seu telefone</label>
                    <input class="form-field" name="cell-number" type="tel"/>
                    <span class="field-message" name="cell-error" id="cell-error"></span>
                </div>
                <div class="control-group">
                    <label class="control-label">Qual o valor disponível para investimentos?</label>
                    <select class="form-field" name="value-investment">
			<option value="">Não Tenho dinheiro disponível</option>
                        <option value="Até R$ 30 mil">Até R$ 30 mil</option>
			<option value="De R$ 30 mil a R$ 100 mil">De R$ 30 mil a R$ 100 mil</option>
			<option value="De R$ 100 mil a R$ 300 mil">De R$ 100 mil a R$ 300 mil</option>
			<option value="De R$ 300 mil a R$ 500 mil">De R$ 300 mil a R$ 500 mil</option>
			<option value="De R$ 500 mil a R$ 1 Milhão">De R$ 500 mil a R$ 1 Milhão</option>
			<option value="De R$ 1 Milhão a R$ 5 Milhões">De R$ 1 Milhão a R$ 5 Milhões</option>
			<option value="Acima de 5 Milhões">Acima de 5 Milhões</option>
                    </select>
                    <span class="field-message"><!-- TODO: Insert error message here. --></span>
                </div>
                <div class="control-group control-check">
                    <label>
                        <input class="form-check" type="checkbox" value=""/>
                        <span>Autorizo o envio das minhas informações a um Agente Autônomo de Investimentos de acordo com os <a href="#" target="_blank">termos de uso</a>.</span>
                    </label>
                </div>
                <div class="control-group form-buttons">
		    <input type="hidden" name="action" value="finance_advice_send" style="display: none; visibility: hidden; opacity: 0;">
                    <button class="btn btn-primary navve-tracker-clk-talk-to-us" id="bt_submit" type="submit">Falar com assessor</button>
                </div>
            </form>
        </section>';

	return $form;

}
?>
