/* Author info */
(function ($) {

    const stopPropagation = function (ev) {
        ev.stopPropagation();
    };

    const toggleAuthor = function (ev) {
        const authorDesc = $('.author-description');
        const body = $('body');
        stopPropagation(ev);
        if(authorDesc.hasClass('visible')) {
            authorDesc.removeClass('visible');
            body.off('click', toggleAuthor);
        } else {
            const avatarPos = $('.author-avatar').offset();
            authorDesc.css({ top: avatarPos.top, left: avatarPos.left }).addClass('visible');
            body.on('click', toggleAuthor);
        }
    };

    $('.author-avatar').on('click', toggleAuthor);
    $('.author-description').on('click', stopPropagation);

})(jQuery);

/* Detect "teste de perfil" links and add css class */
(function ($) {

    const updateLinksCss = function () {
        const links = $('.article-body-content a[href]');
        for(let a = 0, n = links.length; a < n; a++) {
            let el = links[a];
            let address = el.getAttribute('href');
            let matches = 0;
            if(address.indexOf('perfil') != -1) {
                matches++;
            }
            if(address.indexOf('investidor') != -1) {
                matches++;
            }
            if(address.indexOf('teste') != -1) {
                matches++;
            }
            if(matches >= 2 || address.indexOf('/assessoria-de-investimentos') != -1) {
                $(el).addClass('article-text-cta-link');
            }
        }
    };

    $(updateLinksCss);

})(jQuery);


/* Comments */
(function ($) {

    const showComments = function () {
        $('.article-comments').addClass('active').find('.article-comments-title').off('click', showComments);
        $('.article-comments .fb-comments').css('display','block');          
    };

    $('.article-comments-title').on('click', showComments);
    $('.fb-comments').on('click', showComments);    

})(jQuery);


/* Sticky elements */
(function ($) {

    let animId;

    const checkScroll = function (elements) {
        const scrollY = $(window).scrollTop();
        for(let n = elements.length, a = 0; a < n; a++) {
            let el = $(elements.get(a));
            el.toggleClass('sticky-active', el.data('threshold') <= scrollY);
        }
    };

    const callScrollCheck = function (elements) {
        window.cancelAnimationFrame(animId);
        animId = window.requestAnimationFrame(checkScroll.bind(null, elements));
    };

    const setup = function () {
        const stickies = $('.sticky').filter(':visible');
        const n = stickies.length;
        for(let a = 0; a < n; a++) {
            let el = $(stickies.get(a));
            el.data({ threshold: el.offset().top - ($('.site-header').outerHeight() || 96) });
        }
        if(n) {
            $(window).off('scroll').on('scroll', callScrollCheck.bind(null, stickies));
        }
    };

    setup();
    $(window).on('resize', () => {
        window.cancelAnimationFrame(animId);
        animId = window.requestAnimationFrame(setup);
    });

})(jQuery);