(function ($, viewport) {

    const selectors = [
        {
            element: '.nv-section-youtube',
            navigation: '.nv-section-youtube',
            area: '.nv-section-youtube .nv-youtube-teasers-body',
            track: '.nv-section-youtube .nv-youtube-teasers-body-content'
        },
        {
            element: '.nv-section-special',
            navigation: '.nv-section-special',
            area: '.nv-section-special .nv-category-teasers-body',
            track: '.nv-section-special .nv-category-teasers-body-content'
        },
        {
            element: '.nv-categories',
            navigation: '.nv-categories',
            area: '.nv-categories .nv-categories-content',
            track: '.nv-categories .nv-categories-list'
        }
    ];

    const addSliderElements = function () {
        selectors.forEach(sel => {
            $(sel.element).addClass('nv-slider');
            $(sel.track).addClass('nv-slider-track');
            $(sel.area).addClass('nv-slider-area');
            $(sel.navigation).append([
                '<div class="nv-scroller-bts nv-slider-bts">',
                '<div class="nv-scroller-bt-prev nv-slider-prev-button"></div>',
                '<div class="nv-scroller-bt-next nv-slider-next-button"></div>',
                '</div>'
            ].join(''));
        });
    };

    addSliderElements();

})(jQuery, window);
