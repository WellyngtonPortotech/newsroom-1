<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php wp_head(); ?>
</head>
<body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?>>
<?php do_action('after_body_open_tag'); ?>
<div class="nv-login">
    <div class="nv-login-content">
        <div class="nv-login-intro">
            <div class="nv-login-intro-content">
                <h1>Ganhe experiência. Seja referência.</h1>
                <p>Seu conteúdo para milhões de pessoas.</p>
            </div>
        </div>
        <div class="nv-login-form">
            <?php while(have_posts()) { the_post(); the_content(); } ?>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
