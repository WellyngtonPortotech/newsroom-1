<?php
/**
 * Home page
 */

require_once get_template_directory() . '/inc/backend/content-proxy.php';
require_once get_template_directory() . '/inc/blocks-v2/article-teaser.php';
require_once get_template_directory() . '/inc/blocks-v2/category-teasers.php';
require_once get_template_directory() . '/inc/blocks-v2/categories-list.php';
require_once get_template_directory() . '/inc/blocks-v2/youtube-feed.php';
require_once get_template_directory() . '/inc/blocks-v2/newsletter.php';

global $post;

get_header();
?>
<div class="nv-page-main nv-page-main-home">
    <?php categoriesList(); ?>
    <div class="nv-sections-group">
        <div class="nv-section-featured">
            <h1 class="no-display">Destaques</h1>
            <?php
            /* Destaques */
            $slots = array(
                array('section' => 'home-featured-01', 'thumb' => 'nv-home-featured-big'),
                array('section' => 'home-featured-02', 'thumb' => 'nv-home-featured-medium'),
                array('section' => 'home-featured-03', 'thumb' => 'nv-home-featured-medium'),
                array('section' => 'home-featured-04', 'thumb' => 'nv-home-featured-medium')
            );
            foreach($slots as $slot) {
                $posts = ContentProxy::getPosts('home', $slot['section'], 1);
                $wpQuery = $posts['wpQuery'];
                if($wpQuery->have_posts()) {
                    $wpQuery->the_post();
                    articleTeaserBlock($post, '', $slot['thumb']);
                    wp_reset_postdata();
                }
            }
            ?>
            <div class="nv-ad-box">
                <div class="nv-ad nv-ad-mobile-superbanner" id="div-gpt-ad-1605551294224-0"></div>
            </div>
        </div>
        <div class="nv-section-popular">
            <?php
            /* Mais lidas */
            categoryTeasers(
                array('home', 'home-top-five', 5),
                array('nv-home-category-medium')
            );
            ?>
        </div>
    </div>
    <div class="nv-sections-group">
        <div class="nv-section-stocks-widget">
        </div>
        <div class="nv-section-last-news">
            <?php
            /* Últimas notícias */
            categoryTeasers(
                array('home', 'home-last-news', 4),
                array('nv-home-last-news-small')
            );
            ?>
        </div>
    </div>
    <div class="nv-sections-group nv-sections-group-ordered">
        <div class="nv-section-category">
            <?php
            /* Economia */
            categoryTeasers(
                array('home', 'home-category-01', 4),
                array('nv-home-category-big', 'nv-home-category-medium')
            );
            ?>
        </div>
        <div class="nv-section-ad">
            <div class="nv-ad-box nv-ad-mobile">
                <div class="nv-ad nv-ad-medium-rectangle" id="div-gpt-ad-1605551386584-0"></div>
            </div>
            <div class="nv-ad-box nv-ad-desktop">
                <div class="nv-ad nv-ad-large-rectangle" id="div-gpt-ad-1605551740281-0"></div>
            </div>
        </div>
    </div>
    <div class="nv-sections-group">
        <div class="nv-section-category">
            <?php
            /* Mercados */
            categoryTeasers(
                array('home', 'home-category-02', 4),
                array('nv-home-category-big', 'nv-home-category-medium')
            );
            ?>
        </div>
        <div class="nv-section-newsletter">
            <?php
            /* Newsletter */
            newsletterBlock();
            ?>
        </div>
    </div>
    <div class="nv-section-category">
        <?php
        /* Ações */
        categoryTeasers(
            array('home', 'home-category-03', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-ad">
        <div class="nv-ad-box nv-ad-desktop">
            <div class="nv-ad nv-ad-billboard" id="div-gpt-ad-1605551788043-0"></div>
        </div>
    </div>
    <div class="nv-section-special nv-scroller">
        <?php
        /* Especiais */
        categoryTeasers(
            array('home', 'home-category-special', 12),
            array('nv-home-sliders-small')
        );
        ?>
    </div>
    <div class="nv-section-category">
        <?php
        /* Negócios */
        categoryTeasers(
            array('home', 'home-category-04', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-category">
        <?php
        /* Fundos de investimento */
        categoryTeasers(
            array('home', 'home-category-05', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-category">
        <?php
        /* Fundos Imobiliários */
        categoryTeasers(
            array('home', 'home-category-06', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-special nv-scroller">
        <?php
        /* Colunistas */
        categoryTeasers(
            array('home', 'home-category-columnists', 12),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-category">
        <?php
        /* Renda Fixa */
        categoryTeasers(
            array('home', 'home-category-07', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-category">
        <?php
        /* Poupança */
        categoryTeasers(
            array('home', 'home-category-08', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
        <div class="nv-section-category">
        <?php
        /* Planos de Previdência */
        categoryTeasers(
            array('home', 'home-category-09', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
        <div class="nv-section-category">
        <?php
        /* Tesouro Direto */
        categoryTeasers(
            array('home', 'home-category-10', 6),
            array('nv-home-category-big', 'nv-home-category-medium')
        );
        ?>
    </div>
    <div class="nv-section-ad">
        <div class="nv-ad-box nv-ad-desktop">
            <div class="nv-ad nv-ad-super-leaderboard" id="div-gpt-ad-1605551690644-0"></div>
        </div>
        <div class="nv-ad-box nv-ad-mobile">
            <div class="nv-ad nv-ad-medium-rectangle" id="div-gpt-ad-1605551440559-0"></div>
        </div>
    </div>
    <div class="nv-section-youtube nv-scroller">
        <?php
        /* YouTube */
        youtubeTeasers('Nosso canal no YouTube');
        ?>
    </div>
</div>
<?php
get_footer();
?>
