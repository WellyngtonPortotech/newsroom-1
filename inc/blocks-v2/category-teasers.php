<?php
require_once get_template_directory() . '/inc/backend/content-proxy.php';
require_once get_template_directory() . '/inc/blocks-v2/article-teaser.php';

/**
 * Prints a category block with links to posts.
 * @author Lanza
 * @param array $postsParams Parameters to get category posts.
 * @param array $thumbSizes Names of thumbnails image's size (one of the list defined in functions.php).
 * @param string $cssClass CSS class to add in the block.
 * @param string $titleTag Tag to use in section title.
 */
function categoryTeasers($postsParams = array(), $thumbSizes = array('thumbnail'), $cssClass = '', $titleTag = 'h1') {
    global $post;
    $posts = call_user_func_array(array('ContentProxy', 'getPosts'), $postsParams);
    if(count($posts['posts'])){
    ?>
        <section class="nv-category-teasers<?php if($cssClass) echo ' ' . $cssClass; ?>">
            <div class="nv-category-teasers-content">
                <<?php echo $titleTag; ?> class="nv-category-teasers-title">
                    <?php
                    if(is_string($posts['url'])) {
                        ?>
                        <a href="<?php echo esc_attr($posts['url']); ?>"><?php echo esc_html($posts['title']); ?></a>
                        <?php
                    } else {
                        ?>
                        <span><?php echo esc_html($posts['title']); ?></span>
                        <?php
                    }
                    ?>
                </<?php echo $titleTag; ?>>
                <div class="nv-category-teasers-body">
                    <div class="nv-category-teasers-body-content">
                        <?php
                        $wpQuery = $posts['wpQuery'];
                        $a = 0;
                        $lastThumbIndex = max(0, count($thumbSizes) - 1);
                        while($wpQuery->have_posts()) {
                            $wpQuery->the_post();
                            $thumbSize = $thumbSizes[min($a, $lastThumbIndex)];
                            articleTeaserBlock($post, '', $thumbSize);
                            $a++;
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </section>
    <?php
    }
}